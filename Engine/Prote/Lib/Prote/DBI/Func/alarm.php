<?php
namespace Prote\DBI\Func;
use DIC\Service;

class alarm {
    private $Service=NULL;
    public $Db=NULL; 

    public function __construct(Service $Service){
        $this->Service=$Service;
        $this->Db=$this->Service->Database();
    }

    public function add($h,$m){  
        $c=$this->Db->find_many("SELECT * from alarm");
        foreach ($c as $data)
        {
            if($data->hr==$h&&$data->min==$m)
            { 
              echo "Duplication in alarm values....<br>Please wait..";
              header("refresh:3;url=/dashboard");
              return 0;
            }
        }
        $this->Db->set_parameters(array($h,$m));  
        return $this->Db->Insert('INSERT INTO `comments`.`alarm` (`aid`, `hr`, `min`, `done`) VALUES (NULL, ?, ?, 0);');
    }  
    public function get_count(){ 
        if($data=$this->Db->find_one('SELECT aid FROM `alarm`')){
            return $data->aid;
        }else{
            return 0;
        }
    } 
    public function get_htime(){
        $this->Db->set_parameters(array());
        if($data=$this->Db->find_one('SELECT hr FROM `alarm` WHERE done=0 order by hr,min LIMIT 1')){
            return $data->hr;
        }else{
            return 0;
        }
    }  
    public function get_mtime(){
        $this->Db->set_parameters(array());
        if($data=$this->Db->find_one('SELECT min FROM `alarm` WHERE done=0 order by hr,min LIMIT 1')){
            return $data->min;
        }else{
            return 0;
        }
    }        
    public function remove(){
        $this->Db->set_parameters(array());
        if($this->Db->query('DELETE FROM `alarm` WHERE done=1')){
            return 1;
        }else{
            return 0;
        }
    }
    public function removeAllAlarms(){
        $this->Db->set_parameters(array());
        if($this->Db->query('DELETE FROM `alarm` where 1;')){
            return 1;
        }else{
            return 0;
        }
    }

    public function install(){
        $payload1="CREATE TABLE IF NOT EXISTS `alarm` (
                   `aid` int(255) NOT NULL AUTO_INCREMENT,
                   `hr` int(255) NOT NULL,
                   `min` int(25) NOT NULL,
                   `done` int(1) NOT NULL DEFAULT '0',
                   PRIMARY KEY (`aid`)
                   ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;";
        $payloads=(array($payload1));
        $this->Db->drop_payload($payloads,$this);
    }

}