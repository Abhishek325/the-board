<?php
namespace Prote\DBI\Func;
use DIC\Service;

class comment{
    private $Service=NULL;
    public $Db=NULL; 

    public function __construct(Service $Service){
        $this->Service=$Service;
        $this->Db=$this->Service->Database();
    }

    public function add($head,$com,$type){
        $head=preg_replace ("/ +/", " ", $head);
        $com=preg_replace ("/ +/", " ", $com); 
        $sym=array("<",">");
        $map=array("&lt;","&gt;");
        $head=str_replace($sym,$map,$head);   
        $com=preg_replace ("/< *marquee *>/", "&lt;marquee&gt;", $com); 
            
        if($com==" "||$head==" ")
        {
            echo "Invalid Input.<br><b>Redirecting to dashboard.</b><br>"; 
            header("refresh:5;url=/dashboard");
        }
        else
        {
         $c=$this->Db->find_many("SELECT * from diary");
        foreach ($c as $data)
        {
            if($data->header==$head)
            { 
              echo "This header aleady exists.<by>Try something else..<br>Please wait.."; 
              header("refresh:3;url=/dashboard");
              return 0;
            }
        } 
        $this->Db->set_parameters(array($head,$com,$type)); 
        return $this->Db->Insert('INSERT INTO `comments`.`diary` (`cid`, `header`, `text`, `time`, `type`) VALUES (NULL,?,?, NOW(),?);');
        } 
    }   
    public function get_add_time(){
        if($data=$this->Db->find_one('select substring(`time`,12,5) as time from diary')){
            return $data->time;
        }else{
            return 0;
        }
    }
    public function get_access_date_month($cid){
        $this->Db->set_parameters(array($cid)); 
        if($data=$this->Db->find_one('select substring(`time`,6,2) as time  from diary where cid=?')){
            switch($data->time)
            {

                case 1 :return 'January';
                case 2 :return 'February';
                case 3 :return 'March';
                case 4 :return 'April';
                case 5 :return 'May';
                case 6 :return 'June';
                case 7 :return 'July';
                case 8 :return 'August';
                case 9 :return 'September';
                case 10:return 'October';
                case 11:return 'November'; 
                case 12:return 'December';
            }
        }else{
            return 'Invalid data entry';
        }
    }
    
    public function get_access_date_day($cid){
        $this->Db->set_parameters(array($cid)); 
        if($data=$this->Db->find_one('select substring(`time`,9,2) as time from diary where cid=?')){
           return $data->time;
        }else{
            return 'Invalid data entry';
        }
    } 

    public function get_access_year($cid){
        $this->Db->set_parameters(array($cid)); 
        if($data=$this->Db->find_one('select substring(`time`,1,4) as time from diary where cid=?')){
           return $data->time;
        }else{
            return 'Invalid data entry';
        }
    } 
    public function install(){
            $payload1="CREATE TABLE IF NOT EXISTS `diary` (
                      `cid` int(100) NOT NULL AUTO_INCREMENT,
                      `header` varchar(100) NOT NULL,
                      `text` mediumtext NOT NULL,
                      `time` timestamp NOT NULL,
                      `type` varchar(15) NOT NULL,
                      `intensity` int(200) NOT NULL DEFAULT '0',
                      PRIMARY KEY (`cid`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;";
        $payloads=(array($payload1));
        $this->Db->drop_payload($payloads,$this);
    }

    
}