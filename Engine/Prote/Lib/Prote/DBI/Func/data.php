<?php
namespace Prote\DBI\Func;
use DIC\Service;

class data {
    private $Service=NULL;
    public $Db=NULL; 

    public function __construct(Service $Service){
        $this->Service=$Service;
        $this->Db=$this->Service->Database();
    }

    public function add($desc,$text){
        $this->Db->set_parameters(array($desc,$text));
        return $this->Db->Insert('INSERT INTO `comments`.`data` (`cid`, `desc`, `text`, `time`) VALUES (NULL, ?,?, CURRENT_TIMESTAMP);');
    }
 public function get_desc($cid){
        $this->Db->set_parameters(array($cid));
        if($data=$this->Db->find_one('SELECT `desc` FROM `data` WHERE cid=?')){
            return $data->desc;
        }else{
            return 0;
        }
    }

    public function get_text($cid){
        $this->Db->set_parameters(array($cid));
        if($data=$this->Db->find_one('SELECT `text` FROM `data` WHERE cid=?')){
            return $data->text;
        }else{
            return 0;
        }
    } 
    public function get_time($cid){
        $this->Db->set_parameters(array($cid));
        if($data=$this->Db->find_one('SELECT `time` FROM `data` WHERE cid=?')){
            return $data->time;
        }else{
            return 0;
        }
    }
    public function get_access_time(){
        if($data=$this->Db->find_one('select substring(`session_lastaccesstime`,12) as session_lastaccesstime from protesession')){
            return $data->session_lastaccesstime;
        }else{
            return 0;
        }
    } 
    
    public function get_access_date_month(){
        if($data=$this->Db->find_one('select substring(`session_lastaccesstime`,6,2) as session_lastaccesstime  from protesession')){
            switch($data->session_lastaccesstime)
            {

                case 1 :return 'January';
                case 2 :return 'February';
                case 3 :return 'March';
                case 4 :return 'April';
                case 5 :return 'May';
                case 6 :return 'June';
                case 7 :return 'July';
                case 8 :return 'August';
                case 9 :return 'September';
                case 10:return 'October';
                case 11:return 'November'; 
                case 12:return 'December';
            }
        }else{
            return 'Invalid data entry';
        }
    }
    public function get_access_date_day(){
        if($data=$this->Db->find_one('select substring(`session_lastaccesstime`,9,2) as session_lastaccesstime from protesession')){
           return $data->session_lastaccesstime;
        }else{
            return 'Invalid data entry';
        }
    }
    //select substring(`session_lastaccesstime`,12) from protesession
    //select substring(`session_lastaccesstime`,6,2) from protesession
    //select substring(`session_lastaccesstime`,9,2) from protesession
   public function verify($email,$pwd){
        $this->Db->set_parameters(array($email,$pwd));
        if($data=$this->Db->find_one('SELECT `Id` from `admin` where Email=? && Pwd=?')){
            return $data->Id;
        }else{
            return 0;
        }
    }
    public function generate_hash($pwd){
        return $this->hash=hash('sha512',$pwd.$this->salt);
    }

    public function make_salt($email,$pwd){
        return $this->salt=crypt($pwd, $email.'5UCK5'.$pwd);    
    }

    public function addUser($type,$name,$email,$pwd,$pin){
        $Handle=0;
        $login_att=0; 
        $c=$this->Db->find_many("SELECT * from admin");
        foreach ($c as $data)
        {
            if($data->Email==$email)
            { 
              echo "<html lang='en'>
      <head>
      <head>
      <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
      <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no'/>
      <title>Dashboar initializing.</title>
      <link rel='shortcut icon' href='Static/VA/images/joker.jpg'> 
      </script> 
      <link href='Static/VA/css/materialize.css' type='text/css' rel='stylesheet' media='screen,projection'/> 
      <link href='Static/VA/css/custom.css' type='text/css' rel='stylesheet' /> 
      </head>
      <body>
      <div class='navbar-fixed'>
      <nav class='light-red lighten-1' role='navigation'>
      <div class='nav-wrapper container'> 
      <h4 class='brand-logo center light' >Dashboard.</h4>
      <ul class='right hide-on-med-and-down'>
      </ul>
      </div>
      </nav>
      </div>
      <div class='section no-pad-bot' id='index-banner'>
      <div class='container'>
      <br><br><br>
      <div class='row center'>
        <h4 class='header col s12 light' style='margin-top:-50px;'>Sorry for inconvenience !!! </h4> 
        <h5>This mail id is already present in the system.<br>Try some other mail id.<br><br>
        <a href='/system.config' class='small btn'>Go back</a></h5>
      </div>"; 
       exit();
            }
        }
        $this->make_salt($email,$pwd);
        $pwd=$this->generate_hash($pwd); 
        $this->Db->set_parameters(array($type,$name,$email,$pwd,$Handle,$login_att,$pin));
        return $this->Db->Insert('INSERT INTO `comments`.`admin` (`Id`, `type`, `name`,`Email`, `Pwd`, `Handle`, `login_attempt`, `pin`) VALUES (NULL, ?,?,?,?,?,?,?);');
    }
    
    public function getName()
    { 
        if($data=$this->Db->find_one('SELECT name from `admin`;')){
            return $data->name;
        }
        else 
            return 0; 
    }
    public function getEmail()
    { 
        if($data=$this->Db->find_one('SELECT Email from `admin`;')){
            return $data->Email;
        }
        else 
            return 0; 
    }
    
    public function getpin(){ 
        if($data=$this->Db->find_one('SELECT `pin` FROM `admin` LIMIT 1')){
            return $data->pin;
        }
        else 
            return 0; 
    }
    public function getpwd(){ 
        if($data=$this->Db->find_one('SELECT `Pwd` FROM `admin` LIMIT 1')){
            return $data->Pwd;
        }
        else 
            return 0; 
    }
    public function getUserType(){ 
        if($data=$this->Db->find_one('SELECT `type` FROM `admin` LIMIT 1;')){
            return $data->type;
        }
        else 
            return 0; 
    } 
    public function getautologouttime(){
      if($data=$this->Db->find_one('SELECT `autologout` FROM `admin`;')){
            return $data->autologout;
        }
        else 
            return 0;   
    }
    public function set_delflag($f,$id){
        $this->Db->set_parameters(array($f,$id));
        if($this->Db->query('UPDATE `data` SET `delflag`=? WHERE `cid` = ?')){
            return 1;
        }else{
            return 0;
        }
    }
    public function change_pin($new){
        $this->Db->set_parameters(array($new));
        if($this->Db->query('UPDATE `comments`.`admin` SET `pin` = ? WHERE `admin`.`Id`=1')){
            return 1;
        }else{
            return 0;
        }
    }
    public function change_pwd($new){
        $this->Db->set_parameters(array($new));
        if($this->Db->query('UPDATE `data` SET `delflag`=? WHERE `cid` = ?')){
            return 1;
        }else{
            return 0;
        }
    }
    
    
   //UPDATE `comments`.`admin` SET `login_attempt` = '1' WHERE `admin`.`Id` = 1;
   public function  update_fail_count(){
        $this->Db->set_parameters(array());
        if($this->Db->query('UPDATE `admin` SET `login_attempt` = (login_attempt+1) WHERE `admin`.`Id` = 1')){
            return 1;
        }else{
            return 0;
        }
    } 
   public function login_fail_count(){
        $this->Db->set_parameters(array());
        if($data=$this->Db->find_one('SELECT `login_attempt` FROM `admin`')){
            return $data->login_attempt;
        }else{
            return 0;
        }
    }
   
   public function remove(){
        $this->Db->set_parameters(array());
        if($this->Db->query('DELETE FROM `data` WHERE delflag=1')){
            return 1;
        }else{
            return 0;
        }
    }
     public function get_Handle(){
        if($data=$this->Db->find_one('select Handle from admin')){
            return $data->Handle;
        }else{
            return -1;
        }
    } 
    public function set_Handle(){
         $data=$this->Db->find_one('select Handle from admin');
            $v= $data->Handle;
            if($v==0)
               $v=1;
            else
               $v=0; 
        $this->Db->set_parameters(array($v));
        if($this->Db->query('UPDATE `comments`.`admin` SET `Handle` = ? WHERE 1')){
            return 1;
        }else{
            return -1;
        }
    } 
    public function set_err_msg($s){
        $this->Db->set_parameters(array($s));
        if($this->Db->query('UPDATE `comments`.`admin` SET `err_msg` = ? WHERE 1')){
            return $s;
        }else{
            return 0;
        }
    }
    public function update($time)
    {
        $timeConv=$time*60+1;
        $this->Db->set_parameters(array($timeConv)); 
         if($this->Db->query('UPDATE `comments`.`admin` SET `autologout`= ? WHERE `admin`.`Id` = 1;')){
            return $timeConv;
        }else{
            return 0;
        } 
    }
    public function exitSessions()
    {
        if($this->Db->query('DELETE FROM `protesession` WHERE 1')){
            return 1;
        }else{
            return 0;
        }
    } 
    public function install(){
        $payload1=" CREATE TABLE IF NOT EXISTS `admin` (
          `Id` int(255) NOT NULL AUTO_INCREMENT,
          `type` varchar(25) NOT NULL DEFAULT 'sir',
          `name` varchar(255) NOT NULL,
          `Email` varchar(255) NOT NULL,
          `Pwd` text NOT NULL,
          `Handle` int(1) NOT NULL DEFAULT '0',
          `login_attempt` int(1) NOT NULL DEFAULT '0',
          `pin` int(4) NOT NULL,
          `autologout` int(2) NOT NULL DEFAULT '61',
          PRIMARY KEY (`Id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0";

        $payload2="CREATE TABLE IF NOT EXISTS `protesession` (
                   `session_id` varchar(255) NOT NULL,
                   `session_data` text NOT NULL,
                   `session_lastaccesstime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        
        $payloads=(array($payload1,$payload2));
        $this->Db->drop_payload($payloads,$this);
    }
  
}