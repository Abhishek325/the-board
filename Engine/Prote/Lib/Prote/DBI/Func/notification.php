<?php
namespace Prote\DBI\Func;
use DIC\Service;

class notification {
    private $Service=NULL;
    public $Db=NULL; 

    public function __construct(Service $Service){
        $this->Service=$Service;
        $this->Db=$this->Service->Database();
    }
    public function add($noti)
    { 
       $done=0;
       $this->Db->set_parameters(array($noti,$done)); 
       return $this->Db->Insert('INSERT INTO `comments`.`notification` (`id`, `noti`, `done`) VALUES (NULL,?, ?);');

    }
    public function get_count()
    { 
        if($data=$this->Db->find_one('SELECT count(*)  as total FROM `notification`;')){
            return $data->total;
        }else{
            return 0;
        }
    }
    public function get_notification()
    { 
        if($data=$this->Db->find_one('SELECT noti FROM `notification` ;')){
            return $data->noti;
        }else{
            return 0;
        }
    }
    public function remove($id){
        $this->Db->set_parameters(array($id));
        if($this->Db->query('DELETE FROM `notification` WHERE id=?;')){
            return 1;
        }else{
            return 0;
        }
    } 
    public function removeAll(){ 
        if($this->Db->query('DELETE FROM `notification` WHERE 1;')){
            return 1;
        }else{
            return 0;
        }
    } 
    public function install(){
        $payload1="CREATE TABLE IF NOT EXISTS `notification` (
                  `id` int(255) NOT NULL AUTO_INCREMENT,
                  `noti` varchar(255) NOT NULL,
                  `done` int(1) NOT NULL DEFAULT '0',
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;"; 
        $payloads=(array($payload1));
        $this->Db->drop_payload($payloads,$this);
    }
 
}