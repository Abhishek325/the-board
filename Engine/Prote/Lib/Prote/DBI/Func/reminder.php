<?php
namespace Prote\DBI\Func;
use DIC\Service;

class reminder {
    private $Service=NULL;
    public $Db=NULL; 

    public function __construct(Service $Service){
        $this->Service=$Service;
        $this->Db=$this->Service->Database();
    } 
    public function add($text,$h,$m,$L){
        $text=preg_replace ("/ +/", " ", $text);
        $sym=array("<",">");
        $map=array("&lt;","&gt;");
        $text=str_replace($sym,$map,$text);  
        $this->Db->set_parameters(array($text,$h,$m,$L));
        if($text==" ")
        {
          echo "Invalid Input.<br><b>Redirecting to dashboard.</b><br>";
          header("refresh:2;url=/dashboard");
        } 
        else
        {
          $c=$this->Db->find_many("SELECT * from reminder");
        foreach ($c as $data)
        {
            if($data->hour==$h&&$data->minute=$m)
            { 
              echo "Another reminder with same time already exists..<br>Please wait.."; 
              header("refresh:3;url=/dashboard");
              return 0;
            }
        }
        //$text=$this->Html()->secure_post_text($text);  
        return $this->Db->Insert('INSERT INTO `comments`.`reminder` (`rid`, `rname`, `hour`, `minute`, `done`, `link`) VALUES (NULL, ?, ?, ?,0,?);');
        }
    } 
    public function get_text( ){
        $this->Db->set_parameters(array( ));
        if($data=$this->Db->find_one('SELECT `rname` FROM `reminder` LIMIT 1')){
            return $data->rname;
        }else{
            return 0;
        }
    }  
    public function get_link( ){
        $this->Db->set_parameters(array( ));
        if($data=$this->Db->find_one('SELECT `link` FROM `reminder` LIMIT 1')){
            return $data->link;
        }else{
            return 0;
        }
    }    
    public function get_htime(){
        $this->Db->set_parameters(array());
        if($data=$this->Db->find_one('SELECT hour FROM `reminder` WHERE 1 order by rid')){
            return $data->hour;
        }else{
            return 0;
        }
    }  
   
    public function get_mtime(){
        $this->Db->set_parameters(array());
        if($data=$this->Db->find_one('SELECT minute FROM `reminder` WHERE 1 order by rid')){
            return $data->minute;
        }else{
            return 0;
        }
    } 
    public function get_count(){ 
        if($data=$this->Db->find_one('SELECT rname FROM `reminder`')){
            return $data->rname;
        }else{
            return 0;
        }
    } 
   
    public function delete($rid){
        $this->Db->set_parameters(array($rid));
        if($this->Db->query('DELETE FROM `reminder` WHERE rid=?')){
            return 1;
        }else{
            return 0;
        }
    }

    public function remove($h){
        $this->Db->set_parameters(array($h ));
        if($this->Db->query('DELETE FROM `reminder` WHERE hour<?')){
            return 1;
        }else{
            return 0;
        }
    }

    public function install(){
        $payload1="CREATE TABLE IF NOT EXISTS `reminder` (
                 `rid` int(255) NOT NULL AUTO_INCREMENT,
                 `rname` varchar(255) NOT NULL,
                 `hour` int(2) NOT NULL,
                 `minute` int(2) NOT NULL,
                 `done` int(1) NOT NULL DEFAULT '0',
                 `link` varchar(255) NOT NULL,
                 PRIMARY KEY (`rid`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;";
        $payloads=(array($payload1));
        $this->Db->drop_payload($payloads,$this);
    }
  
}