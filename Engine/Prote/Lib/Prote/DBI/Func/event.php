<?php
namespace Prote\DBI\Func;
use DIC\Service;

class event {
    private $Service=NULL;
    public $Db=NULL; 

    public function __construct(Service $Service){
        $this->Service=$Service;
        $this->Db=$this->Service->Database();
    }

    public function add($name,$day,$month,$type){
        $name=str_replace(",","<br>",$name);
        $name=preg_replace ("/ +/", " ", $name);
        if($name==" ")
        {
            echo "Invalid name for the event.";
            exit();
        }
        $sym=array("<",">");
        $map=array("&lt;","&gt;");
        $name=str_replace($sym,$map,$name);  
        $name=str_replace("'","&quot;",$name);  
        $this->Db->set_parameters(array($name,$day,$month,$type));
        return $this->Db->Insert('INSERT INTO `comments`.`event` (`eid`, `ename`, `day`, `month`,`type`) VALUES (NULL,?,?,?,?);');
    } 

    public function getEvents($day,$month){
        $this->Db->set_parameters(array($day,$month));
        if($data=$this->Db->find_one('SELECT `ename` as event FROM `event` WHERE day=? and month=?;')){
            return $data->event;
        }else{
            return 0;
        }
    } 
    public function getEventCount($day,$month){
        $this->Db->set_parameters(array($day,$month));
        if($data=$this->Db->find_one('SELECT count(`ename`) as count FROM `event` WHERE day=? and month=?;')){
            return $data->count;
        }else{
            return 0;
        }
    } 
    public function getEventType($id){
        $this->Db->set_parameters(array($id));
        if($data=$this->Db->find_one('SELECT `type` from event where id=? ')){
            return $data->type;
        }else{
            return 0;
        }
    } 
    public function getNextEventDay($day,$month){ 
        $this->Db->set_parameters(array($day,$month,$day,$month)); 
        if($data=$this->Db->find_one('SELECT day FROM `event` WHERE (day>? and month=?) or (day<? and month>?) LIMIT 1;')){
            return $data->day;
        }else{
            return -1;
        }
    }
    public function getNextEventMonth($day,$month){ 
        $this->Db->set_parameters(array($day,$month,$day,$month)); 
        if($data=$this->Db->find_one('SELECT month FROM `event` WHERE (day>? and month=?) or (day<? and month>?) LIMIT 1;')){
            return $data->month;
        }else{
            return -1;
        }
    }
    public function delEvents($day,$month){
        $this->Db->set_parameters(array($day,$month,"one_time"));
        if($this->Db->query('DELETE from event where (day<? or month<?) and type like ?;')){
            return 1;
        }else{
            return 0;
        }
    } 
    public function install(){
        $payload1="CREATE TABLE IF NOT EXISTS `event` (
                  `eid` int(255) NOT NULL AUTO_INCREMENT,
                  `ename` varchar(255) NOT NULL,
                  `day` int(2) NOT NULL,
                  `month` int(2) NOT NULL,
                  `type` varchar(100) NOT NULL,
                  PRIMARY KEY (`eid`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;";

        $payloads=(array($payload1));
        $this->Db->drop_payload($payloads,$this);
    }
}