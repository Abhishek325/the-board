<?php
namespace Prote\DBI\Func;
use DIC\Service;

class activity {
    private $Service=NULL;
    public $Db=NULL; 

    public function __construct(Service $Service){
        $this->Service=$Service;
        $this->Db=$this->Service->Database();
    }

    public function add($activity){
        $this->Db->set_parameters(array($activity));
        return $this->Db->Insert('INSERT INTO `comments`.`activity` (`id`, `act_des`, `time`) VALUES (NULL,?, CURRENT_TIMESTAMP);');
    } 

    public function remove(){ 
         if($this->Db->query('delete from activity where 1;')){
            return 1;
        }else
            return 0;   
    }
    public function countActivity(){ 
         if($data=$this->Db->find_one('SELECT count(*) as count from activity;')){
            return $data->count;
        }
        else 
            return 0; 
    } 

    public function install(){
    	$payload1="CREATE TABLE IF NOT EXISTS `activity` (
 				    `id` int(255) NOT NULL AUTO_INCREMENT,
 				    `act_des` varchar(255) NOT NULL,
 				    `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 				    PRIMARY KEY (`id`) 
				   ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;";
          $payloads=(array($payload1));
        $this->Db->drop_payload($payloads,$this);
    }

}