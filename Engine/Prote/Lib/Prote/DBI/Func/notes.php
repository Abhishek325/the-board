<?php
namespace Prote\DBI\Func;
use DIC\Service;

class notes {
    private $Service=NULL;
    public $Db=NULL; 

    public function __construct(Service $Service){
        $this->Service=$Service;
        $this->Db=$this->Service->Database();
    }

    public function add($text){  
       $text=preg_replace ("/ +/", " ", $text);//All multispaces converted to single space.
       //Save the planet.  Prevent injections.
       $sym=array("<div>","</div>","<script>","</script>");
       $map=array("&lt;div&gt;","&lt;/div&gt;","&lt;script&gt;","&lt;/script&gt;");
       $text=str_replace($sym,$map,$text);
       $text=preg_replace ("/< *script *>/", "&lt;script&gt;", $text);
       $text=preg_replace ("/< *\/+script *>/", "&lt;/script&gt;", $text); // \div
       $text=preg_replace ("/< *div *.*>/", "&lt;div&gt;", $text);//div   
       $text=preg_replace ("/< *\/+div *>/", "&lt;/div&gt;", $text); // \div
       $text=preg_replace ("/< *h/", "&lt;h", $text);     //Headers
       $text=preg_replace ("/< *\/+h/", "&lt;/h", $text);     
       $text=preg_replace ("/< *p/", "&lt;p", $text);     //paragraphs
       $text=preg_replace ("/< *\/+p/", "&lt;/p", $text);
       $text=preg_replace ("/< *input/", "&lt;input", $text); //inputs 
       $text=preg_replace ("/.*margin/", " ", $text); //inputs 
       $text=preg_replace ("/\"+/", "&quot;", $text); //multiple occurences of quotes.
       if($text==" ")
       {
         echo "Invalid Input.<br><b>Redirecting to dashboard.</b><br>"; 
          return 0;
       } 
        else
        { 
        $this->Db->set_parameters(array($text)); 
        return $this->Db->Insert('INSERT INTO `comments`.`todo` (`id`, `text`, `time`, `delflag`) VALUES (NULL, ?, CURRENT_TIMESTAMP, 0);');
        }
    }
    public function get_access_time(){
        if($data=$this->Db->find_one('select substring(`time`,12) as time from todo')){
            return $data->time;
        }else{
            return 0;
        }
    } 
    
    public function get_access_date_month(){
        if($data=$this->Db->find_one('select substring(`time`,6,2) as time  from todo')){
            switch($data->time)
            {

                case 1 :return 'January';
                case 2 :return 'February';
                case 3 :return 'March';
                case 4 :return 'April';
                case 5 :return 'May';
                case 6 :return 'June';
                case 7 :return 'July';
                case 8 :return 'August';
                case 9 :return 'September';
                case 10:return 'October';
                case 11:return 'November'; 
                case 12:return 'December';
            }
        }else{
            return 'Invalid data entry';
        }
    }
    public function get_access_date_day(){
        if($data=$this->Db->find_one('select substring(`time`,9,2) as session_lastaccesstime from todo')){
           return $data->time;
        }else{
            return 'Invalid data entry';
        }
    } 
   public function set_delflag($f,$id){
        $this->Db->set_parameters(array($f,$id));
        if($this->Db->query('UPDATE `todo` SET `delflag`=? WHERE `id` = ?')){
            return 1;
        }else{
            return 0;
        }
    }
    public function getcount(){ 
        if($data=$this->Db->find_one('select count(id) as count from todo')){
            return $data->count;
        }else{
            return 0;
        }
    }
   //UPDATE `comments`.`admin` SET `login_attempt` = '1' WHERE `admin`.`Id` = 1;

    /*   public function prompt(){
        $msg='<form action="/restore" method="post"><button type="submit"  style="color:#000;font-weight:500;text-decoration:none;background:none;border:none;">Restore</button></form>' ;
        $this->Db->set_parameters(array($msg));
        $this->Db->query('UPDATE `comments`.`todo` SET `temp` = `text` WHERE `todo`.`delflag` = 1');
        if($this->Db->query('UPDATE `comments`.`todo` SET `text` = ? WHERE `todo`.`delflag` = 1')){
            return 1;
        }else{
            return 0;
        }
    }
     public function restore(){
        $this->Db->set_parameters(array());
        if($this->Db->query('UPDATE `comments`.`todo` SET `text` = `temp` WHERE `todo`.`delflag` = 1')){
            return 1;
        }else{
            return 0;
        }
    }  */
   public function remove(){
     $this->Db->query('UPDATE `comments`.`admin` SET `value` =`value`+3  WHERE 1');
        $this->Db->set_parameters(array());
        if($this->Db->query('DELETE FROM `todo` WHERE delflag=1')){
            return 1;
        }else{
            return 0;
        }
    } 
    public function get_Handle(){
        if($data=$this->Db->find_one('select Handle from admin')){
            return $data->Handle;
        }else{
            return -1;
        }
    } 
    public function set_Handle(){
         $data=$this->Db->find_one('select Handle from admin');
            $v= $data->Handle;
            if($v==0)
               $v=1;
            else
               $v=0; 
        $this->Db->set_parameters(array($v));
        if($this->Db->query('UPDATE `comments`.`admin` SET `Handle` = ? WHERE 1')){
            return 1;
        }else{
            return -1;
        }
    }

    public function install(){
        $payload1="CREATE TABLE IF NOT EXISTS `todo` ( 
                   `id` int(255) NOT NULL AUTO_INCREMENT,
                   `text` text NOT NULL,
                   `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                   `delflag` int(1) NOT NULL DEFAULT '0',
                   `temp` text NOT NULL,
                   PRIMARY KEY (`id`)
                  ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;";
        $payload2="INSERT INTO `todo` (`id`, `text`, `time`, `delflag`, `temp`) VALUES
                 (1, 'Welcome to the Dashboard.','', 0, ''),
                 (2, 'This is where you can keep your notes.', '', 0, '')";
        $payloads=(array($payload1,$payload2));
        $this->Db->drop_payload($payloads,$this);
    }
 
}