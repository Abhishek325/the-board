 <?php
date_default_timezone_set('Asia/Kolkata'); 
 echo " 
<html lang='en'>
<head>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no'/>
  <title>".$Service->Prote()->DBI()->Func()->data()->getName()." Online.</title>
  <link rel='shortcut icon' href='/Static/VA/images/joker.jpg'>
  <link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>
</script>
  <!-- CSS  -->
  <link href='/Static/VA/css/materialize.css' type='text/css' rel='stylesheet' media='screen,projection'/>
  <link href='/Static/VA/css/custom.css' type='text/css' rel='stylesheet' />
  <script type='text/javascript' src='/Static/VA/js/jquery.min.js'></script>
</head> 
<div onmouseover='resetDormancy()' onclick='resetDormancy()' onkeypress='resetDormancy()'>
<ul id='slide-out' class='side-nav fixed'>
  <li style='background:#EE6E73;;color:#fff;'><a href='#!' style='color:#fff;font-size:21px;'>";
  if($Service->Prote()->DBI()->Func()->notes()->get_Handle()==1)
   echo "<u>#</u>";
  else
   echo "<u>$</u>";
  echo $Service->Prote()->DBI()->Func()->data()->getName()." </a></li> 
  <li><div class='card' style='height:320px;'>
        <div class='card-image waves-effect waves-block waves-light'>
          <img class='activator' src='/Static/VA/images/joker.jpg'>
        </div>
        <div class='card-content'>
          <span class='card-title activator grey-text text-darken-4'>".$Service->Prote()->DBI()->Func()->data()->getName()." <i class='mdi-navigation-more-vert right'></i></span>
        </div>
        <div class='card-reveal'>
          <span class='card-title grey-text text-darken-4'>Profile picture<i class='mdi-navigation-close right'></i></span>
          <p>This is my profile photo. Coz an image is worth thousand words.</p><form action='#'>
      <!--<input type='file'><br>
    <button type='submit' class='btn-flat waves-effect waves-light green' style='color:#fff;margin-top:1rem;'>Done !!</button>-->
  </form><p></p>
        </div>
      </div></li>
  <li>
  <ul class='collapsible  collapsible-accordion' data-collapsible='accordion'>
          <li class='' style='background:none;'>
            <div class='collapsible-header'><i class='material-icons' style='margin-left: -2rem;margin-top: 0.5rem;'>vpn_key</i><a href='#!'>#Lock | Unlock </a></div>
            <div class='collapsible-body' style='display: none;background:none;'>
             <form action='/disdel' method='post'> 
              <div class='input-field col s10 center'>
              <input type='password' name='val' placeholder='****' class='validate' required style='text-align:center;margin-top:-1.6rem;margin-left:-0.7rem;border-bottom:2px solid #666;color:#000;font-size:22px;width:55%;' autocomplete='off' >
              </div>
              <div class='input-field col s10 center'>
               <button type='submit' class='btn' style='background:#EE6E73;margin-top:-1.5rem;margin-left:-0.7rem;'>";
              if($Service->Prote()->DBI()->Func()->notes()->get_Handle()==1)
                echo "Lock";
              else
               echo "Unlock";
              echo "</button></div><br>
              </form> 
            </div>
          </li>  
        </ul> 
   </li>
  <li><a class='modal-trigger' href='#event' ><i class='mdi-action-home left'></i><b><u>Add event</u></b></a><div id='eventstat' style='margin-left:3rem;margin-top:-1rem;color:#797676;'>
  </div>  
  </li>      
  <li>
   <a class='modal-trigger' href='#reminder' style='margin-bottom:-1rem;'><i class='mdi-action-home left'></i><b><u>Add Reminder</u></b></a>
   <div id='rem' style='margin-left:3rem;'></div> 
   <div style='float:right;margin-top:-1.5rem;'>";
   if($Service->Prote()->DBI()->Func()->reminder()->get_count())
   { 
     $c=$Service->Database()->find_many("SELECT * from reminder");
     foreach ($c as $data)
     {  
     echo "<form action='/delrem' method='post'>
     <input type='hidden' value='".$data->rid."' name='reminderid'>
     <button type='submit' style='background:none;border:none;'>
      <b>X</b>
     </button>
     </form>
     </div><br> ";
     break;//For displaying one reminder at a time.
     } 
   }
   echo " 
  </li>
  <li><a href='#post' class='modal-trigger'><i class='mdi-action-home left'></i>Create post</a></li>
  <li><a href='#html' class='modal-trigger'><i class='mdi-action-home left'></i>HTML test</a></li>  
 </ul>
<ul id='dropdown1' class='dropdown-content'> 
  <li><a href='/settings'>Settings</a></li>  
  <li><a href='/activity.log'>Activity</a></li>   
  <li class='divider'></li>
  <li>
   <form action='/logout' method='post'>
    <input type='hidden' value='".$Service->Html()->auth_token."' name='auth_token'>
    <input type='submit' value='logout'/ style='background:none;font-size: 1.2rem;color:#26a69a;display:block;padding: 1.2rem 1rem;border-width:0;width:100%;float:left;text-align:left;'>
   </form>
  </li>
</ul>
<ul id='dropdown3' class='dropdown-content'>";
$c=$Service->Prote()->DBI()->Func()->notification()->get_count();
if($c) 
{ 
     $c=$Service->Database()->find_many("SELECT * from notification");
     foreach ($c as $data)
     {
      echo "<form action='/delnoti' method='post'><input type='hidden' value='".$data->id."' name='notid'> 
      <li style='font-size: 18px;rem;color: #26a69a;display: block;padding: 1rem 1rem;'>
      ".$data->noti; 
      if($Service->Prote()->DBI()->Func()->notes()->get_Handle()==1)
       echo "<button type='submit' style='background:none;border:none;'><b style='color:#000;font-size:13px;'>X</b></button></li></form>";  
     } 
     if($Service->Prote()->DBI()->Func()->notification()->get_count()>3)
       echo "<form action='/delAllnoti' method='post'><li style='font-size:17px;rem;color:#26a69a;display:block;padding:0.5rem 1rem;color:#fff;background:#E6373E;'><button type='submit' style='background:none;border:none;'>Remove all</button></li></form>";
}
echo "</ul>
<div class='navbar-fixed'>
<nav class='light-red lighten-1' role='navigation'>
  <div class='nav-wrapper'>
    <h4 class='brand-logo center light' >Dashboard";  
  if($Service->Prote()->DBI()->Func()->notes()->get_Handle()==1)
   echo "<a href='/lock-the-system-for-reed-access-only'>.</a>";
  else 
    echo ".";
  echo " </h4> 
	<a href='#' data-activates='mobile-demo' class='button-collapse'><i class='mdi-navigation-menu'></i></a>
    <ul class='right hide-on-med-and-down'> ";
    if($Service->Prote()->DBI()->Func()->notification()->get_count())
     echo "<li><a class='dropdown-button' href='#!' data-activates='dropdown3'>Notifications(<b>".$Service->Prote()->DBI()->Func()->notification()->get_count()."</b>)<i class='mdi-navigation-arrow-drop-down right'></i></a></li>";
      echo "<!-- Dropdown Trigger --> 
      <li><a class='dropdown-button' href='#!' data-activates='dropdown1'>Switches<i class='mdi-navigation-arrow-drop-down right'></i></a></li>
    </ul>
  </div>
  <ul class='side-nav' id='mobile-demo'>  
		<li style='background:#EE6E73;;color:#fff;'><a href='#!' style='color:#fff;font-size:21px;'>";
  if($Service->Prote()->DBI()->Func()->notes()->get_Handle()==1)
   echo "<u>#</u>";
  else
   echo "<u>$</u>";
  echo $Service->Prote()->DBI()->Func()->data()->getName()." </a></li> 
  <li><div class='card' style='height:310px;'>
        <div class='card-image waves-effect waves-block waves-light'>
          <img class='activator' src='/Static/VA/images/joker.jpg'>
        </div>
        <div class='card-content'>
          <span class='card-title activator grey-text text-darken-4'>".$Service->Prote()->DBI()->Func()->data()->getName()." <i class='mdi-navigation-more-vert right'></i></span>
        </div>
        <div class='card-reveal'>
          <span class='card-title grey-text text-darken-4'>Profile picture<i class='mdi-navigation-close right'></i></span>
          <p>This is my profile photo. Coz an image is worth thousand words.</p><form action='#'>
      <!--<input type='file'><br>
    <button type='submit' class='btn-flat waves-effect waves-light green' style='color:#fff;margin-top:1rem;'>Done !!</button>-->
  </form><p></p>
        </div>
      </div></li>
  <li>
  <ul class='collapsible  collapsible-accordion' data-collapsible='accordion'>
          <li class='' style='background:none;'>
            <div class='collapsible-header'><a href='#!'>#Lock | Unlock </a></div>
            <div class='collapsible-body' style='display: none;background:none;'>
             <form action='/disdel' method='post'> 
              <div class='input-field col s10 center'>
              <input type='password' name='val' placeholder='****' maxlength='4' class='validate' required style='text-align:center;margin-top:-1.6rem;margin-left:-0.7rem;border-bottom:2px solid #666;color:#000;font-size:22px;width:55%;' autocomplete='off' >
              </div>
              <div class='input-field col s10 center'>
               <button type='submit' class='btn' style='background:#EE6E73;margin-top:-1.5rem;margin-left:-0.7rem;'>";
              if($Service->Prote()->DBI()->Func()->notes()->get_Handle()==1)
                echo "Lock";
              else
               echo "Unlock";
              echo "</button></div><br>
              </form> 
            </div>
          </li>  
          <li><a href='/#event'><i class='mdi-action-home left'></i><b><u>Add event</u></b></a><div id='eventstat' style='margin-left:3rem;'></div></li>      
  <li>
   <a class='modal-trigger' href='#reminder' style='margin-bottom:-1rem;'><i class='mdi-action-home left'></i><b><u>Add Reminder</u></b></a>
   <div id='rem' style='margin-left:3rem;'></div> 
   <div style='float:right;margin-top:-1.5rem;'>";
   if($Service->Prote()->DBI()->Func()->reminder()->get_count())
   { 
     $c=$Service->Database()->find_many("SELECT * from reminder");
     foreach ($c as $data)
     {  
     echo "<form action='/delrem' method='post'>
     <input type='hidden' value='".$data->rid."' name='reminderid'>
     <button type='submit' style='background:none;border:none;'>
      <b>X</b>
     </button>
     </form>
     </div><br> ";
     break;//For displaying one reminder at a time.
     } 
   }
   else echo "<div style='float:left;color:#222;'>No reminders.</div>";
   echo " 
  </li>
 <li style=''><a href='/settings'>Settings</a></li>
		<li class='divider'></li>
		<li style=''>
      <form action='/logout' method='post'>
    <input type='hidden' value='".$Service->Html()->auth_token."' name='auth_token'>
    <input type='submit' value='Logout'  style='background:none;border:none;color: #444;display: block;font-size: 1rem;height: 64px;line-height: 64px;padding: 0 15px;'>
   </form>
    </li>
  </ul>
</nav>
</div>
  <div class='section no-pad-bot' id='index-banner'>
    <div class='container'>
      <br><br><br>
    </div>
  </div> 
    <body onLoad='startTime();showDay();sdate();reminder();alarmF();activityAnalyzer();'> 
         <div class='row center' >  
            <div class='col s12' style=''>
           <div class='row center' style='margin-top:-5rem;float:right;width:12%;'>
           <h4 class='header col s12 light' style='color:#000;'><div id='time'></div></h4>  
           <h5 class='header col s12 light' style='color:#000;margin-top:-0.9rem;font-size:22px;font-weight:300;'><div id='day'></div></h5>
           <h5 class='header col s12 light' style='color:#000;margin-top:-0.6rem;font-size:20px;font-weight:300;'><div id='date1'></div></h5>
          <a class='btn-large waves-effect waves-light modal-trigger' style='background-color:#EE6E73;' href='#modal1'>SiteData</a>   
       <!--post-->
      <div id='post' class='modal' style='box-shadow:none;'>
      <div class='col s12' style='background:#ee6e73;padding-top:10px;margin-bottom:2rem;'>  
      <h4 style='float:left;color: #fff;font-weight: 300;'>CommentOut.</h4>
     </div>
     <div class='modal-content' >
     <!--Starts--> 
     <form action='/addcomment' method='post'>
     <p><b>A bit of formalities. </b><br>Somebody is watching you..always.<br>Last loaded at <strong>".date('H').":".date('i')."</strong> hours.</p>
      <a href='/technical' class='btn-flat' style='float:right;margin-top:-2.5rem;background-color:F08A8E;color:#fff;border:1px solid #666;margin-right:5.5rem;'>View</a>
      <button  type='submit' onclick='setdata()' class='btn-flat' style='float:right;margin-top:-2.5rem;background-color:F08A8E;color:#fff;border:1px solid #666;'> <i class='mdi-content-send Large' ></i></button>
      <input type='text' name='title' placeholder='Title of the record.' class='col s6' autocomplete='off' required>
      <select class='browser-default col s6' name='type' style='border:none;border-bottom:1px solid #9e9e9e;color:#222;font-weight:400;'>
               <option value='technical'>Technical</option>
               <option value='personal' >Personal</option>
               <option value='site' >Site</option>
              </select> 
     <textarea name='comment' id='real' hidden></textarea>        
     <div class='textarea' id='tar' contenteditable='true' style='text-align:left;background:#F08A8E;color:#fff;border-width:0;padding:7px;height:12rem;width:100%;font-weight:400;margin-top:4.25rem;overflow-y:scroll;'></div>
     </form> 
     <img src='/Static/VA/images/smileys/cool.png' alt='smiley' draggable='false' style='margin-top:-0.5rem;height:20px;' onclick=\"add('cool')\"/>
     <img src='/Static/VA/images/smileys/cry.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('cry')\"/>
     <img src='/Static/VA/images/smileys/embarassed.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('embarassed')\"/>
     <img src='/Static/VA/images/smileys/foot-in-mouth.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('foot-in-mouth')\"/> 
     <img src='/Static/VA/images/smileys/frown.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('frown')\"/>
     <img src='/Static/VA/images/smileys/innocent.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('innocent')\"/>
     <img src='/Static/VA/images/smileys/kiss.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('kiss')\"/>
     <img src='/Static/VA/images/smileys/laughing.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('laughing')\"/>
     <img src='/Static/VA/images/smileys/sealed.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('sealed')\"/> 
     <img src='/Static/VA/images/smileys/smile.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('smile')\"/>
     <img src='/Static/VA/images/smileys/surprised.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('surprised')\"/>
     <img src='/Static/VA/images/smileys/tongue.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('tongue')\"/>
     <img src='/Static/VA/images/smileys/undecided.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('undecided')\"/>
     <img src='/Static/VA/images/smileys/wink.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('wink')\"/>
     <img src='/Static/VA/images/smileys/yell.png' alt='smiley' draggable='false'  style='margin-top:-0.5rem;height:20px;' onclick=\"add('yell')\"/>
     <br>
     <!--Ends --></div>
      </div>
       <!-- ends-->
       <div id='html' class='modal' style='box-shadow:none;'>
      <div class='col s12' style='background:#ee6e73;padding-top:10px;margin-bottom:2rem;'>  
      <h4 style='float:left;color: #fff;font-weight: 300;'>LiveHTML.</h4>
     </div>
     <div class='modal-content' >
     <!--Starts--> 
     <form action='/html' method='post'>
     <p><b>Testing can be done here. </b><br>Last loaded at <strong>".date('H').":".date('i')."</strong> hours.</p> 
       <button  type='reset' class='btn-flat' style='float:right;margin-top:-2.5rem;background-color:F08A8E;color:#fff;border:1px solid #666;margin-right:10rem;'>Clear</button>
      <button  type='submit' class='btn-flat' style='float:right;margin-top:-2.5rem;background-color:F08A8E;color:#fff;border:1px solid #666;'>Save <b>|</b> Open</button>
      <input type='text' name='filename' placeholder='Name of the file.' class='col s6' autocomplete='off'>
     <textarea name='code' style='background:#F08A8E;color:#fff;border-width:0;padding:7px;height:12rem;font-weight:400;'></textarea>
     </form> 
     <!--Ends -->
      <p align='left'><strong>Files are actually allocated on the disk.</strong> Enter <b>Remove</b> in code space to remove the file.</p>
      </div>
      </div>
       <!-- ends-->   
       <!--modal thought-->
       <div id='thought' class='modal' style='box-shadow:none;'>
      <div class='col s12' style='background:#ee6e73;padding-top:10px;margin-bottom:2rem;'>  
      <h4 style='float:left;color: #fff;font-weight: 300;'>Thoughts.</h4>
      </div>
    <div class='modal-content'>
     <!--Starts--> 
      <form action='/addthought' class='col s12 row center' method='post'>
        <input type='text' name='thought' placeholder='Thought/Quote.' class='validate' required style='border-bottom-color:#F08A8E;font-size:21px;text-align:center;' autocomplete='off' > <br>
        <input type='text' name='author'  placeholder='Author ' class='validate' required style='border-bottom-color:#F08A8E;font-size:21px;width:55%;text-align:center;' autocomplete='off' > 
       <br><button type='submit' class='btn waves-effect waves-light' onclick='DisplaySubmitText();valRem();' style='background-color:#ee6e73;margin-top:1rem;' >Add thought</button>
      </form>   
     <!--Ends -->
      </div>
      </div>
   <!--Modal ends-->
   <!--Events-->
   <div id='event' class='modal' style='box-shadow:none;'>
      <div class='col s12' style='background:#ee6e73;padding-top:10px;margin-bottom:2rem;'>  
      <h4 style='float:left;color: #fff;font-weight: 300;'>EventHandler</h4>
     </div>
     <div class='modal-content' >
     <!--Starts--> 
     <form action='/event' method='post' name='myform'> 
     <p>Please enter event details here. You can enter multiple events for the same day. In order to add multiple event for same day, separate each event with a comma (,) while writing in event name field.</p>
        <input type='text' name='etext' placeholder='Event text/Name' class='validate' required='' style='border-bottom-color:#F08A8E;width:55%;font-size:21px;text-align:center;' autocomplete='off'> <br>
        <select class='browser-default' name='emonth' id='emonth' onchange='setdays()' style='width:150px;padding:2px;height:35px;color:#222;font-weight:400;display:inline;border:1px solid #999;' required>
       <option value='0'>Select month..</option>
       <option value='1'>January</option>
       <option value='2'>February</option>
       <option value='3'>March</option>
       <option value='4'>April</option>
       <option value='5'>May</option>
       <option value='6'>June</option>
       <option value='7'>July</option>
       <option value='8'>August</option>
       <option value='9'>September</option>
       <option value='10'>October</option>
       <option value='11'>November</option>
       <option value='12'>December</option>
      </select><select class='browser-default' name='eday' id='mys' style='width:50px;padding:2px;height:35px;color:#222;font-weight:400;display:inline;border:1px solid #999;' required></select><select class='browser-default' name='etype' style='width:120px;padding:2px;height:35px;color:#222;font-weight:400;display:inline;border:1px solid #999;' required>
       <option value='one_time'>One time event</option>
       <option value='all_time'>All time event</option>
      </select>&nbsp; <a href='#' class='btn waves-effect' onclick='settoday()' style='padding:1px 10px;'>Tomorrow</a></b> 
        <br><button type='submit' class='btn waves-effect waves-light' onclick='DisplaySubmitText();valRem();' style='background-color:#ee6e73;margin-top:1rem;'>Submit</button>
      </form> 
     <!--Ends -->
      </div>
      </div>
       <!-- ends-->   
       <!--modal thought-->
       <div id='thought' class='modal' style='box-shadow:none;'>
      <div class='col s12' style='background:#ee6e73;padding-top:10px;margin-bottom:2rem;'>  
      <h4 style='float:left;color: #fff;font-weight: 300;'>Thoughts.</h4>
      </div>
    <div class='modal-content'>
     <!--Starts--> 
      <form action='/addthought' class='col s12 row center' method='post'>
        <input type='text' name='thought' placeholder='Thought/Quote.' class='validate' required style='border-bottom-color:#F08A8E;font-size:21px;text-align:center;' autocomplete='off' > <br>
        <input type='text' name='author'  placeholder='Author ' class='validate' required style='border-bottom-color:#F08A8E;font-size:21px;width:55%;text-align:center;' autocomplete='off' > 
       <br><button type='submit' class='btn waves-effect waves-light' onclick='DisplaySubmitText();valRem();' style='background-color:#ee6e73;margin-top:1rem;' >Add thought</button>
      </form>   
     <!--Ends -->
      </div>
      </div>
   <!--Modal ends-->
   <!--End-->    
       <!--modal rem-->
       <div id='reminder' class='modal' style='box-shadow:none;'>
      <div class='col s12' style='background:#ee6e73;padding-top:10px;margin-bottom:2rem;'>  
      <h4 style='float:left;color: #fff;font-weight: 300;'>Reminders</h4>
      </div>
    <div class='modal-content'>
     <!--Starts--> 
      <form action='/remind' class='col s12 row center' method='post'>
        <input type='text' name='rtext' placeholder='Reminder text/Name ' class='validate' required style='border-bottom-color:#F08A8E;width:55%;font-size:21px;text-align:center;' autocomplete='off' > <br>
        <input type='number' name='hour' value='".date('H')."' placeholder='Hours' class='validate' required style='border-bottom-color:#F08A8E;width:100px;font-size:21px;text-align:center;' autocomplete='off' >
        <b style='font-size:22px;'> : </b>
        <input type='number' name='min' value='".date('i')."' placeholder='Minutes' class='validate' required style='border-bottom-color:#F08A8E;width:100px;font-size:21px;text-align:center;' autocomplete='off' ><br>
        <input type='text' name='linkname' value='http://'   placeholder='Action link to be opened' class='validate' required style='border-bottom-color:#F08A8E;width:400px;font-size:20px;text-indent:1rem;' autocomplete='off' >
        <br><button type='submit' class='btn waves-effect waves-light' onclick='DisplaySubmitText();valRem();' style='background-color:#ee6e73;margin-top:1rem;' >Submit</button>
      </form>     
     <!--Ends -->
      </div>
      </div>
   <!--Modal ends--> 
   <!-- Modal Structure -->
<div id='modal-speak' class='modal' style='box-shadow:none;'>
      <div class='col s12' style='background:#ee6e73;padding-top:10px;margin-bottom:2rem;'>  
      <h4 style='float:left;color: #fff;font-weight: 300;'>SpeakIt</h4>
      </div>
    <div class='modal-content'>
     <!--Starts-->
      <div class='row'>
       <div id='speech'>
    <span id='labnol' contenteditable='true'>Click Start and speak.</span> 
    <span id='notfinal'></span> 
    <span id='warning'></span> 
  </div><br>    
    <span id='messages'> 
      <br><a href='#' class='btn waves-effect waves-light' onclick='javascript:action();return false;' id='btn' style='background:#ee6e73;'>Loading..</a> 
      <a href='#' class='btn waves-effect waves-light' onclick='javascript:clearSlate();return false;' id='btnClear' style='background:#ee6e73;'>Clear</a>
    </span>   
<span class='language'><select name='lang' id='lang' onchange='updateLang(this)'><optgroup label='English' ><option value='6'>Australia</option><option value='7'>Canada</option>
<option value='8'>India</option><option value='9'>New Zealand</option><option value='10'>South Africa</option>
<option value='11'>United Kingdom</option><option value='12'>United States</option></optgroup><optgroup label='--'></select></span> 
  <img id='status' src='/Static/VA/images/listen.gif' style='margin:auto'/>
          </div>
     <!--Ends -->
      </div>
      </div>
   <!--Modal ends--> 

  <div id='modal1' class='modal' style='background:none;box-shadow:none;'>
    <div class='modal-content'>
     <!--Starts-->
      <div class='row'>
           <div class='col s12'>
           <div class='card-panel teal z-depth-3' style='border:1px solid #AB5054;width:100%;padding:6px;padding-top:3px;border-radius:4px;padding-bottom:3px;text-align:left;font-size:16px;box-shadow:none;float:right'>
           <span class='white-text'> 
           <h5 style='font-weight:300;'>&nbsp;&nbsp;SiteData</h5>
           <form action='/store' class='col s12' method='post'>
           <div class='input-field col s10'>
           <input type='text' name='website' id='web' class='validate' required style='border-bottom-color:#F08A8E;' autocomplete='off' >
           <label for='web'style=' color:#F08A8E;'>Website URL</label>
           </div>
           <div class='input-field col s10'>
           <input type='text' name='email' id='email' class='validate' required style='margin-top:-1rem;border-bottom-color:#F08A8E;' autocomplete='off' >
           <label for='email'style='margin-top:-1rem; color:#F08A8E;'>Email/Username</label>
           </div>
           <div class='input-field col s10'>
           <input type='password' name='pwd' id='password' style='margin-top:-1rem;border-bottom-color:#F08A8E;' class='validate' required   >
           <label for='password' style='margin-top:-1rem;color:#F08A8E;'>Password</label>
           </div>
           <div class='input-field col s12'>
           <button onclick='DisplayClearText()' type='reset' class='btn-flat col s3 offset-s11' style='margin-top:-3.5rem;background:none;float:right;color:#F08A8E;width:20%;' >X</button>
           <button onclick='DisplaySubmitText()' type='submit' class='btn-flat col s3 offset-s11' style='margin-top:-3.5rem;background:none;float:right;margin-right:-1.0rem;width:10%;' > 
           <i class='mdi-content-send Large' style='margin-left:-55px;color:#F08A8E;'></i></button> <br>
           </div>
           </form> 
           </div> 
           </div> 
           </div> 
     <!--Ends -->
     <script type='text/javascript'>
     function DisplayClearText()
     {
      Materialize.toast('Cleared all the fields', 2000);  
     }
     function DisplaySubmitText()
     {
      Materialize.toast('Validating the values..', 1000);  
     }  
     function valRem()
     {
      var hrin=document.getElementByName('hour');
      var minin=document.getElementByName('min');
      if(hrin<".date('H').")
       Materialize.toast('Invalid Hour value', 10000);  
      else if(hrin==".date('H').")
        if(minin<=".date('i').")
          Materialize.toast('Invalid Minute value', 10000);  
     }
     </script>
      </div>
      </div>
   <!--Modal ends--> 
   <!-- Modal 2-->
    <div id='modal2' class='modal' style='background:none;box-shadow:none;'>
    <div class='modal-content'>
     <div class='row center' style='border:1px solid #ee6e73;'>
      <div class='col s12' style='background:#ee6e73;padding-top:10px;'>  
      <h4 style='float:left;color: #fff;font-weight: 300;'>FastAccess</h4>
      <div class='col s12' style='background:#ee6e73;padding-top:10px;'>  
      <form action='/addsite' method='post'>
          <input type='text' name='web' class='col s4' style='border-bottom-color:#F08A8E;margin-top:-1rem;font-size:21px;color:#fff;font-weight:300;' placeholder='Add new here.'  autocomplete='off' required >
          <input type='text' name='weburl' class='col s6' style='border-bottom-color:#F08A8E;margin-top:-1rem;font-size:21px;color:#fff;font-weight:300;margin-left:5rem;' ' placeholder='url' value='http://'  autocomplete='off' required >
          <button type='submit' onclick='DisplaySubmitText();' class='btn-flat col s3 offset-s11' style='margin-top:-3.5rem;background:none;float:right;margin-right:-0.5rem;width:40px;' > 
          <i class='mdi-content-send Large' style='margin-left:-55px;color:#F08A8E;float:right;'></i></button>
          </form>
      </div>
      </div>
     ";
      $c=$Service->Database()->find_many("SELECT * from web order by freq");
      $count=0; 
          foreach ($c as $data)
          { 
            echo "<a type href='".$data->url."' target='_new' style='color:#fff;font-weight:300;'><div class='col s4  waves-effect waves-light ' style='padding:1rem;background-color:#F75B61;padding-left:1px;margin-top:2px;'>".$data->name."</div></a>";  
          }
           
     echo "  
     </div>
     </div>
     </div>
   <!--Ends -->
    <a  class='btn-large waves-effect waves-light modal-trigger' style='background-color:#EE6E73;margin-top:1px;' href='#modal-speak'>&nbsp;Speak It </a>
   <a class='btn-large waves-effect waves-light dropdown-button' data-activates='dropdown2' style='background-color:#EE6E73;margin-top:1px;' href='#modal1'>Websites</a>
     <ul id='dropdown2' class='dropdown-content'> 
  <li><a href='http://www.google.com' target='_new'>Google</a></li>
  <li><a href='http://acharyacse.in' target='_new'>Acharya CSE</a></li>
  <li><a href='http://mymailer.coolpage.biz' target='_new'>TheMailer</a></li>
  <li><a href='#modal2' class='modal-trigger' style='color:#000;'>more..</a></li>
  </ul>
  </div>
  </div> 
           <div class='col s10 '>     
           <div class='card-panel teal z-depth-3' style='margin-left:auto;margin-right:0rem;width:65%;margin-top:-19.5rem;padding:6px;
           padding-top:3px;padding-bottom:3px;text-align:left;border-top:15px solid #EE6E73;float:right;font-size:16px;box-shadow:none;overflow-y:scroll;height:270px;overflow-x:hidden;'>
          <span class='white-text'> 
           <h5 style='font-weight:300;'>&nbsp;Notes (".$Service->Prote()->DBI()->Func()->notes()->getcount().")</h5>
          <ul><form action='/del' method='post'>"; 
          $c=$Service->Database()->find_many("SELECT * from todo");
          $count_notes=$Service->Prote()->DBI()->Func()->notes()->getcount();
          if($count_notes==0)
            echo "<li style='margin:4px;background:rgba(255, 255, 255, 0.2);padding:7px;font-weight:300;border-radius:4px;'>No more notes to display</li>";
          else
          foreach ($c as $data)
          {  
           echo "<li id='".$data->id."'  style='margin:4px;background:rgba(255, 255, 255, 0.2);padding:7px;font-weight:300;border-radius:4px;'>".$data->text;
           if($Service->Prote()->DBI()->Func()->notes()->get_Handle()==1)
            echo "<input type='hidden' value='".$data->id."' name='id'><button type='submit' onmouseover=\"line('".$data->id."')\" onmouseout=\"lineoff('".$data->id."')\" style='float:right;background:none;color:222;border:0;'>x</button></li></form><form action='/del' method='post' style='margin-top:-0.8rem;'>";
          }
          echo"
          </form></ul>";
          echo "
          </span>
          </div>
          </div>
          <!--
          <div class='col s6'>
          <div class='card-panel teal z-depth-3' style='width:75%;margin-top:-19.5rem;padding:6px;
           padding-top:3px;padding-bottom:3px;text-align:left;font-size:16px;box-shadow:none;margin-bottom:1rem;overflow-y:scroll;height:300px;overflow-x:hidden;'>
          <span class='white-text'> 
           <h5 style='font-weight:300;'>&nbsp;Comments</h5>
          <ul><form action='/delc' method='post'>"; 
          $c=$Service->Database()->find_many("SELECT * from data");
          foreach ($c as $data)
          {
           echo "<li style='margin:4px;background:rgba(255, 255, 255, 0.2);padding:7px;font-weight:300;border-radius:4px;'>".$data->text;
          if($Service->Prote()->DBI()->Func()->data()->get_Handle()==1)
            echo "<input type='hidden' value='".$data->cid."' name='id'><button type='submit' style='float:right;background:none;color:222;border:0;'>x</button></li></form><form action='/delc' method='post' style='margin-top:-0.8rem;'>";
          }
          echo"</form></ul>";
         if($Service->Prote()->DBI()->Func()->notes()->get_Handle()==1)
          echo " 
          <form action='/addcom' method='post'>
          <input type='text' name='comment' style='margin-top:-0.2rem;border-bottom-color:#F08A8E;margin-left:0.5rem;text-indent:2px;width:95%;'  autocomplete='off' required >
          <button type='submit' class='btn-flat col s3 offset-s11' style='margin-top:-3.5rem;background:none;float:right;margin-right:-0.9rem' > 
          <i class='mdi-content-send Large' style='margin-left:-55px;color:#F08A8E;float:right;'></i></button>
          </form>";
          echo "</span>
          </div>
          </div>
          ";
          echo "</span>
          </div>
          </div>-->
          </div> 

          <div class='row'>
          <div class='col s12'></div>
          <div class='col s10' style='margin-left:auto;margin-right:0rem;margin-top:-3.5rem;'>
          <div class='card-panel teal z-depth-3' style='width:65%;margin-left:auto;height:45px;padding:7px;box-shadow:none;'>
           <span class='white-text'> ";
            if($Service->Prote()->DBI()->Func()->notes()->get_Handle()==1)
          echo "
          <form action='/addnote' method='post'>
          <input type='text' placeholder='Notes' name='todo' style='margin-top:-0.2rem;border-bottom-color:#F08A8E;height:2rem;margin-left:0.5rem;text-indent:2px;width:95%;z-index:1;'  autocomplete='off' required >
          <button type='submit' class='btn-flat col s3 offset-s11' style='margin-top:-3.5rem;background:none;float:right;margin-right:-0.8rem' > 
          <i class='mdi-content-send Large' style='margin-left:-55px;color:#343434;float:right;'></i></button>
          </form>";
           echo "</span>
          </div>
          </div> 
          </div> 
          
          <div class='row center'>   
          <div class='col  s4' style='margin-left:8rem;' >
           <div class='card-panel teal z-depth-3' style='postion:absolute;text-align:left;width:70%;margin-top:-1.5rem;padding:6px;padding-top:3px;padding-bottom:3px; float:right;font-size:16px;'>
           <span class='white-text'> 
           <h5 style='font-weight:300;'>&nbsp;StopWatch</h5>
           <div id='clockstatus' style='float:right;margin-top:-2rem;font-weight:300;'>Ready</div>
            <div style='margin:4px;padding:7px;font-weight:300;border-radius:4px;margin-top:-0.5rem;margin-bottom:1rem;'>
             <h3 style='font-weight:300;text-align:center;' id='StopWatch'>00:00:00</h3>
             <div class='center'>
              <button id='start' class='btn-flat waves-effect' style='background:#eee;font-weight:400; text-align:left;padding-left:7px;padding-right:7px;' onclick='init();'>Start</button>
              <button id='pause' class='btn-flat waves-effect' style='background:#eee;font-weight:400; text-align:left;padding-left:7px;padding-right:7px;' onclick='PauseClock();'>Pause</button>
              <button id='stop' class='btn-flat waves-effect' style='background:#eee;font-weight:400; text-align:left;padding-left:7px;padding-right:7px;' onclick='stopTheClock();'>Stop</button>
             </div>
            </div>
           </span> 
           </div>
          </div>  
          <div class='col  s4' style='margin-left:-8rem;' >
           <div class='card-panel teal z-depth-3' style='text-align:left;width:70%;margin-top:-1.5rem;padding:6px;padding-top:3px;padding-bottom:3px; float:right;font-size:16px;'>
           <span class='white-text'> 
           <h5 style='font-weight:300;'>&nbsp;Alarm</h5>
           <div id='almstatus' style='float:right;margin-top:-2rem;font-weight:300;'></div>
            <div style='margin:4px;padding:7px;font-weight:300;border-radius:4px;margin-top:-0.5rem;margin-bottom:1rem;'>
             <h4 style='font-weight:300;text-align:center;' id='alarm'></h4> 
             <form action='/addalarm' method='post'>
              Set alarm time:
              <input type='number' name='ah' value='".date('H')."' style='font-weight:400;width:50px;height:30px;text-indent:5px;background-color:#fff;color:#000;'><b> :</b>
              <input type='number' name='am' value='".(date('i')+1)."' style='font-weight:400;width:50px;height:30px;text-indent:5px;background-color:#fff;color:#000;'>
              <button  class='btn-flat waves-effect' style='background:#eee;font-weight:400; text-align:left;padding-left:7px;padding-right:7px;margin-bottom:-1rem;float:right;height:33px;' type='submit'>Set</button> 
             </form>
             <form style='margin-top:-1.5rem;' action='/alarm' method='post'>
             Set after
              <input type='number' name='offset' required value='01' style='font-weight:400;width:50px;height:30px;text-indent:5px;background-color:#fff;color:#000;'>
               <select class='browser-default' name='drop' style='float:right;width:150px;height:32px;color:#222;font-weight:400;'>
               <option value='m'>Minutes</option>
               <option value='h' >Hour(s)</option>
              </select> 
              <button  class='btn-flat waves-effect' style='background:#eee;font-weight:400; text-align:left;padding-left:7px;padding-right:7px;margin-top:-0.8rem;float:right;height:33px;' type='submit'>Set</button> 
             </form>
             <form action='/delalarm' method='post'>";
             if($Service->Prote()->DBI()->Func()->alarm()->get_count()!=0)
              echo "<button  class='btn-flat waves-effect' style='background:#eee;font-weight:400; text-align:left;padding-left:7px;padding-right:7px;margin-top:-0.5rem;float:right;height:33px;margin-top:-1.9rem;margin-right:0.2rem;' type='submit'>Dismiss</button>";
             else
              echo "<button  disabled class='btn-flat waves-effect' style='background:#eee;font-weight:400; text-align:left;padding-left:7px;padding-right:7px;margin-top:-0.5rem;float:right;height:33px;margin-top:-1.9rem;margin-right:0.2rem;background-color:#d1d1d1;' type='submit'>Dismiss</button>";
             echo "</form>
            </div>
           </span> 
           </div>
          </div>  
          <div class='col  s4' style='margin-left:-8rem;' >
           <div class='card-panel teal z-depth-3' style='text-align:left;width:70%;height:200px;margin-top:-1.5rem;padding:6px;padding-top:3px;padding-bottom:3px; float:right;font-size:16px;'>
           <span class='white-text'> 
           <h5 style='font-weight:300;'>&nbsp;ThoughtProcess</h5>
            <div style='float:right;margin-top:-2rem;font-weight:400;'> <a class='modal-trigger' href='#thought' style='color:#fff;'>Add (+)</a>&nbsp;</div>
            <div style='margin:4px;padding:7px;font-weight:300;border-radius:4px;margin-top:-0.5rem;margin-bottom:1rem;'>
              <div class='row' style= background:rgba(255, 255, 255, 0.2);'><em>&quot;</em> ".$Service->Prote()->DBI()->Func()->thought()->get_Random_thought().". <em>&quot;</em></div>
            </div>
           </span> 
           </div>
          </div> 
          </div>
 
           <div class='row' style='margin-top:-1rem;'>
           <div class='col s6' style='margin-left:1rem;'>
           <div class='card-panel teal z-depth-3' style='width:65%;background:url(Static/VA/images/weather.png) no-repeat;background-size:cover;height:280px;float:right;'>
            <b>Weather Forecast<br>
            City: Bangalore<br>    
            Date: ".date('d')."/".date('m')."/".date('y') ." 
            </b>
           </div>
           </div>
           <div class='card-panel teal z-depth-3' style='width:40%;height:280px;float:left;margin-left:-0.8rem;overflow-y:scroll;'>
            <span class='white-text' style='font-size:21px;font-weight:300;'> 
             Weather report<b> :</b>
            </span><hr>
            <span class='white-text' style='font-size:17px;font-weight:300;'> ";
            $city="Bangalore";
            $country="IN"; //Two digit country code
            $url="http://api.openweathermap.org/data/2.5/weather?q=".$city.",".$country."&units=metric&cnt=7&lang=en";
            $network=$Service->Prote()->DBI()->Func()->custom()->check_url($url);
            if($network)
            { 
             $json=file_get_contents($url);
             $data=json_decode($json,true);
             $data_dump="<b>Temperature : </b>".$data['main']['temp']."<br><b>Condition : </b>".$data['weather'][0]['main']."<br><b>% cloud : </b>".$data['clouds']['all']."<br><b>Wind speed : </b>".$data['wind']['speed']."<br>";
             if($Service->Prote()->DBI()->Func()->custom()->remove())//remove old and add new:ROAN :)
             {
              $Service->Prote()->DBI()->Func()->custom()->add($data_dump);
             }
             //Get current Temperature in Celsius
             echo "Displaying weather report <b>:</b><br><br><b>Temperature : </b>".$data['main']['temp']." &deg;C<br>";
             //Get weather condition
             echo "<b>Condition : </b>".$data['weather'][0]['main']."<br>";
             //Get cloud percentage
             echo "<b>% cloud : </b>".$data['clouds']['all']."<br>";
             //Get wind speed
             echo "<b>Wind speed : </b>".$data['wind']['speed']."<br>"; 
           } 
           else
           { 
            echo "<strong>No network !!!<br>Please refresh the page once you are connected to the network.</strong><br>
             <div style='font-size:16px;'>Displaying last fetched data from the server.<br>
             Loaded at <em>". $Service->Prote()->DBI()->Func()->custom()->get_time()."</em><br>".
            $Service->Prote()->DBI()->Func()->custom()->display_old_data()."</div>";
           }
            echo "</span>
           </div><!--The first most div ends here-->
      <audio id='audio' src='/Static/VA/sounds/reminder.mp3' loop ></audio>
      <audio id='hour' src='/Static/VA/sounds/hour.mp3'></audio>
      </body>       
<!--  Scripts-->
  <script src='/Static/VA/js/materialize.js'></script>
  <script src='/Static/VA/js/date_time.js'></script>
  <script src='/Static/VA/js/dictation.js'></script>
  <script src='/Static/VA/js/main.js'></script>  
<script>
 $('.modal-trigger').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
    } 
  );
  $(document).ready(function(){
    $('.collapsible').collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
  });
   $('.button-collapse').sideNav(); 
  $(document).ready(function() {
    $('select').material_select();
  });
  var al=0,alon=0;
  var dormantCount=".$Service->Prote()->DBI()->Func()->data()->getautologouttime().";
  function alarmF()
  {  
    var today=new Date();
    var h1= '".$Service->Prote()->DBI()->Func()->alarm()->get_htime()."';
    
    var m1='".$Service->Prote()->DBI()->Func()->alarm()->get_mtime()."';
    var h=today.getHours();
    var m=today.getMinutes();
    h1=checkTime(h1);
    m1=checkTime(m1);
    ";
    if($Service->Prote()->DBI()->Func()->alarm()->get_count())
    {       
        echo "document.getElementById('almstatus').innerHTML='@<b>'+h1+':'+m1; 
       if(h==h1&&m==m1)
       {
        al=0;
        if(alon==0)
        {   
         Materialize.toast('Alarm Activated.',59000 );
         alon=1;
        }
         document.getElementById('almstatus').innerHTML='Alarm On.';
       }
       else if(h1>=h&&m1<m)
       {";
         $Service->Database()->query('UPDATE `comments`.`alarm` SET `done` = 1 WHERE hr<="'.date('H').'" and min<"'.date('i').'";'); 
         if($Service->Prote()->DBI()->Func()->alarm()->remove())
         {
          echo "
          alon=0;
          if(al==0)
          {  
           Materialize.toast('Deleted alarms with done status.',30000 ); 
           al=1; 
          }
          ";  
         }
         else
          echo "Materialize.toast('Unable to delete old alarms!!!.',5000 );";
         echo "   
       }
       "; 
    }
    else
      echo "document.getElementById('almstatus').innerHTML='No Alarm.';";
    echo " 
  }    
  var showF=0;
  var showNotification=0;
  var eveflag=0;
  var autoLogoutf=0;
  function reminder()
  {
    var today=new Date();
    var h1= '".$Service->Prote()->DBI()->Func()->reminder()->get_htime()."';
    var m1='".$Service->Prote()->DBI()->Func()->reminder()->get_mtime()."';
    var s1=00;
            var h=today.getHours();
            var m=today.getMinutes();
            var s=today.getSeconds();   
            if((m1-m)>0)
             document.getElementById('rem').innerHTML='<b> ".$Service->Prote()->DBI()->Func()->reminder()->get_text()."  </b><br>'+ 'ready in '+ ( m1-m) +' minutes<br>' ;
           else
             document.getElementById('rem').innerHTML='No Reminders. ';
            m=checkTime(m);
            s=checkTime(s);
            if((h==h1&&m1==m)||(h==h1&&(m1-m)==0))
            {
              if(showNotification==0)
              { ";   
               if($Service->Prote()->DBI()->Func()->reminder()->get_text()!=null)
               { 
                echo "Materialize.toast('Reminder to ".$Service->Prote()->DBI()->Func()->reminder()->get_text()."', 57000);
                var audio = document.getElementById('audio');
                audio.play(); 
                showNotification=1;";
               }
               if($Service->Prote()->DBI()->Func()->reminder()->get_link()!="http://")
               { 
                echo "Materialize.toast('opening ".$Service->Prote()->DBI()->Func()->reminder()->get_link()."', 3000);
                $(document).ready(function(){
                 window.open('".$Service->Prote()->DBI()->Func()->reminder()->get_link()."', '_blank'); 
                }); 
                ";
               }
              echo " }
              showF=0;
              document.getElementById('rem').innerHTML='<b> ".$Service->Prote()->DBI()->Func()->reminder()->get_text()."  </b><br>'+ ' &nbsp;is activated<br>'; 
            }
            else if((m>m1)||(h>h1))
            {
             showNotification=0;  
             "; 
             if($Service->Prote()->DBI()->Func()->reminder()->get_count())
            echo "  
               if(h>h1)
               {
                if((m-m1)<0)
                 document.getElementById('rem').innerHTML='No Reminders. ';  
                else  
                document.getElementById('rem').innerHTML='<b><em>".$Service->Prote()->DBI()->Func()->reminder()->get_text()." </em></b><br>'+(h-h1)+'h and '+(m-m1)+' minutes ago';
               }
               else 
               { 
                document.getElementById('rem').innerHTML='<b><em>".$Service->Prote()->DBI()->Func()->reminder()->get_text()." </em></b><br>'+(m-m1)+' minutes ago';
               }
            }";
           else 
              echo "document.getElementById('rem').innerHTML='No Reminders. ';
            }";

            echo " 
            else if(h>h1)
              {".   
               $Service->Prote()->DBI()->Func()->reminder()->remove(date('H')).
             "
             if(showF==0)
             { 
              Materialize.toast('Deleted expired reminders.', 60000);
              showF=1;  
             }
           }
            //Event check goes here.. 
            ";
             $nextEventDay=$Service->Prote()->DBI()->Func()->event()->getNextEventDay(date('d'),date('m')); 
             $nextEventMonth=$Service->Prote()->DBI()->Func()->event()->getNextEventMonth(date('d'),date('m')); 

             if($Service->Prote()->DBI()->Func()->event()->getEventCount($nextEventDay,$nextEventMonth)==1)
             { 
              if($eventName=$Service->Prote()->DBI()->Func()->event()->getEvents($nextEventDay,$nextEventMonth))  
                echo "document.getElementById('eventstat').innerHTML='<font color=\"#333\">Coming event @".($nextEventDay)." '+mapmonth('".$nextEventMonth."')+' <b>:</b></font><br><b>".$eventName."</b>';";  
             }
             else if($Service->Prote()->DBI()->Func()->event()->getEventCount($nextEventDay,$nextEventMonth)>1)
             {
              echo "var events='<font color=\"#333\">Coming events @".$nextEventDay." '+mapmonth('".$nextEventMonth."')+'</font><ul>';";
              $c=$Service->Database()->find_many("select `ename` from event where day=".($nextEventDay)." and month=".$nextEventMonth.";");
              foreach ($c as $data)
              {
               echo "events=events+'<li style=\"margin-left:-1rem;list-style-type:disc;\">".$data->ename."</li>';";
              }

              echo "document.getElementById('eventstat').innerHTML='<b>'+events+'</b></ul>';";
             }
             //today's events:
             if($Service->Prote()->DBI()->Func()->event()->getEventCount(date('d'),date('m')))
             {
               
              if($eventName=$Service->Prote()->DBI()->Func()->event()->getEvents(date('d'),date('m')))  
              {
                $eventName=str_replace("&lt;br&gt;", "<br>&raquo; ", $eventName);
                echo "document.getElementById('eventstat').innerHTML='<font color=\"#333\">Today&rsquo;s event(s) <!--@".(date("d"))." '+mapmonth('".date("m")."')+' --><b>:</b></font><br>&raquo; <b>".$eventName."</b>';";  
              }
              else
              {
                //finally...no today's event(s) or no upcoming events(s)..:|
                echo "document.getElementById('eventstat').style.color=\"#000\";";
                echo "document.getElementById('eventstat').innerHTML='No upcoming events.';" ;
              }
             }
             //Auomated event deletion:
             echo " if(eveflag==0)";
             if($Service->Prote()->DBI()->Func()->event()->getEventCount(date('d')-1,date('m'))&&$Service->Prote()->DBI()->Func()->event()->delEvents(date('d'),date('m')))
             { 
              echo "Materialize.toast('Removed finished events of today.',10000);eveflag=1;"; //?4
             }
             echo "t=setTimeout(function(){reminder();alarmF();},500);  
  } 

  function resetDormancy()
  {
    if(dormantCount<10)
     Materialize.toast('Woaah..that was really close. <img src=\"/Static/VA/images/smileys/wink.png\"><img src=\"/Static/VA/images/smileys/cool.png\">',5000);
    dormantCount=". $Service->Prote()->DBI()->Func()->data()->getautologouttime()."; 
  }
  var commentData='';
  function add(Value)
  {  
     commentData=document.getElementById('tar').innerHTML+'<img src=/Static/VA/images/smileys/'+Value+'.png style=\"display:inline;height:20px;\"/>'; 
     document.getElementById('tar').innerHTML+='<img src=/Static/VA/images/smileys/'+Value+'.png style=\"margin-top:2px;display:inline\">'; 
  }
  function setdata()
  {
   document.getElementById('real').value=commentData+document.getElementById('tar').innerHTML; 
  }
  function line(id)
  { 
    document.getElementById(id).style.textDecoration='line-through';
    document.getElementById(id).style.fontWeight='600';
   }
  function lineoff(id)
  { 
    document.getElementById(id).style.textDecoration='none';
    document.getElementById(id).style.fontWeight='400';
   }
</script> 
</html>";
?>