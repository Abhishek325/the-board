 <html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>This site.</title>
    <link rel="stylesheet" href="/Static/VA/css/foundation.css" /> 
    <link rel="shortcut icon" href="/Static/VA/images/diary.png" type="image/x-icon">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <div class="row">
      <div class="large-12 columns">
        <h1 align="center">WordsOWiz.</h1><br>
      </div>
    </div>
    <style>
    .panel{
      font-family: 'Open Sans', sans-serif;
    }
    </style> 
	<div class="row">
      <div class="large-8 medium-8 columns">
         <div class="row">
          <div class="large-12 columns">
            <?php
             $c=$Service->Database()->find_many("SELECT * from diary where `type`='site' order by cid DESC");
             if($c)
             { 
              foreach ($c as $data)
               {
                   echo "<section id='".$data->header."'></section>";
                   echo "<div class='callout panel'><h5>".$data->header."</h5>".$data->text."<strong>".
                  substr($data->time,10,6)." hrs, ". $Service->Prote()->DBI()->Func()->comment()->get_access_date_month($data->cid)." ".$Service->Prote()->DBI()->Func()->comment()->get_access_date_day($data->cid).", ".$Service->Prote()->DBI()->Func()->comment()->get_access_year($data->cid)."</strong> </div>";
               }
             } 
             else
              echo "<div class='callout panel'><h5>No post to view.</h5></div>";
            ?> 
          </div>
        </div>
        </div>   
    <div class="large-4 medium-4 columns"> 
		<div class="panel" style="background:#fff;">
        <ul>
         <li><h5><a href="/technical">Technical</a></h5></li> 
        </ul> 
    </div>
    <div class="panel" style="background:#fff;">
      <h4>My posts</h4><hr>
    <?php
             $c=$Service->Database()->find_many("SELECT * from diary where `type`='site' order by cid DESC;");
             if($c)
             { 
              echo "<ul>";
              foreach ($c as $data)
               {
                   echo "<li><h5><a href='#".$data->header."' style='color:#5170c2;'>".$data->header."</a></h5></li>" ;
               }
               echo "</ul>";
             } 
             else
              echo "<h5>No post to view.</h5>";
    ?>    
    </div> 
    </div>
    </div> 
  </body> 
</html>
