 <html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Tehnical stuff</title>
    <link rel="stylesheet" href="/Static/VA/css/foundation.css" /> 
    <link rel="shortcut icon" href="/Static/VA/images/diary.png" type="image/x-icon">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <div class="row">
      <div class="large-12 columns">
        <h1 align="center">TechOWiz.</h1><br>
      </div>
    </div>
    <style>
    .panel{
      font-family: 'Open Sans', sans-serif;
    }
    </style> 
	<div class="row">
      <div class="large-8 medium-8 columns">
         <div class="row">
          <div class="large-12 columns">
            <?php
             $c=$Service->Database()->find_many("SELECT * from diary where `type`='technical' order by cid DESC");
             if($c)
             { 
              foreach ($c as $data)
               {
                   echo "<section id='".$data->header."'></section>";
                   echo "<div class='callout panel'><h5>".$data->header."</h5>";
                   if(strlen($data->text)>600)
                    echo substr($data->text,0,450)."<strong><a href='/technical/$data->header' style='text-shadow:2px 3px 2px #ccc;font-weight:600;font-size:16px;'>Read &raquo;</a><br>";
                   else
                    echo $data->text."<strong>";  
                 echo  substr($data->time,10,6)." hrs, ". $Service->Prote()->DBI()->Func()->comment()->get_access_date_month($data->cid)." ".$Service->Prote()->DBI()->Func()->comment()->get_access_date_day($data->cid).", ".$Service->Prote()->DBI()->Func()->comment()->get_access_year($data->cid)."</strong> </div>";
               }
             } 
             else
              echo "<div class='callout panel'><h5>No post to view.</h5></div>";
            ?> 
          </div>
        </div>
        </div>   
      <div class="large-4 medium-4 columns"> 
      <div class="panel" style="background:#fff;">
        <ul>
         <li><h5><a href="/dev-site">Site development</a></h5></li> 
        </ul> 
    </div>
 <div class="panel" style="background:#fff;">
      <h4>My posts</h4><hr>
    <?php
             $c=$Service->Database()->find_many("SELECT * from diary where `type`='technical' order by cid DESC;");
             if($c)
             { 
              echo "<ul>";
              foreach ($c as $data)
               {
                   echo "<li><h5><a href='#".$data->header."' style='color:#5170c2;'>".$data->header."</a></h5></li>" ;
               }
               echo "</ul>";
             } 
             else
              echo "<h5>No post to view.</h5>";
    ?>    
    </div> 
    </div> 
  </body>
</html>
