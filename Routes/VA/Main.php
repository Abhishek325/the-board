 <?php 
date_default_timezone_set('Asia/Kolkata');
/**
 * @package Routes
 * @Caution Heavy use of closures Ahead
 **/
// $Service->enable_route('VA/User');
//URGENT!! URGENT!! URGENT!!
//Clean up these routes. 

$Router->get('/',function() use ($Service) {
	include('Views/VA/index.html');
});

$Router->get('/now',function() use ($Service) {
	include('Views/VA/now.php');
});

$Router->get('/dashboard',function() use ($Service) {
	if($Service->Auth()->logged_in())
	 include('Views/VA/dash.php');
	else
	 header("location:/login") ;
}); 

$Router->get('/settings',function() use ($Service) {
	if($Service->Auth()->logged_in()){
		include('Views/VA/settings.html');
	}
	else{
		header("location:/dashboard");
	}
}); 

$Router->get('/login',function() use ($Service) {
	if($Service->Auth()->logged_in()){ 
		header("location:/dashboard");
	}
	else{
		include($Service->Config()->get_basepath().'/Views/VA/login.html');
	}
});
 
$Router->post('/login',function() use ($Service) {
	if($Service->Auth()->login($_POST['email'],$_POST['pwd'],$Service->Html()->auth_token))
	{
		    /*$referer=$_SERVER['HTTP_REFERER'];
			echo "Welcome <br>";
			echo "<a href='".$referer."'> Go back.</a>";
			echo "<a href='/dashboard'> Dashboard.</a>";
			echo "<form action='/logout' method='post'><input type='hidden' value='".$Service->Html()->auth_token."' name='auth_token'>
			<input type='submit' value='logout'/></form>";*/
			header("location:/dashboard");
	}
		else{

             //echo $Service->Prote()->DBI()->Func()->data()->update_fail_count()."<br>";
             //echo $Service->Prote()->DBI()->Func()->data()->login_fail_count();
			//echo "No momos for you bro";
			$act="<b>Security breach.Login attempt.</b>";
            $Service->Prote()->DBI()->Func()->activity()->add($act);
            $notification="Login attempt<br>@".date('H').":".date('i');
            $Service->Prote()->DBI()->Func()->notification()->add($notification); 
			header("location:/login");
		    }
});
$Router->post('/setup',function() use ($Service) { 
	if($_POST['email']&&$_POST['password']&&$_POST['pin'])
	{  
	if($Service->Prote()->DBI()->Func()->data()->addUser($_POST['type'],$_POST['name'],$_POST['email'],$_POST['password'],$_POST['pin']))
	{    
      echo "
      <html lang='en'>
	  <head>
 	  <head>
  	  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  	  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no'/>
  	  <title>Dashboar initializing.</title>
  	  <link rel='shortcut icon' href='Static/VA/images/joker.jpg'> 
	  </script> 
  	  <link href='Static/VA/css/materialize.css' type='text/css' rel='stylesheet' media='screen,projection'/> 
  	  <link href='Static/VA/css/custom.css' type='text/css' rel='stylesheet' /> 
	  </head>
      <body>
      <div class='navbar-fixed'>
  	  <nav class='light-red lighten-1' role='navigation'>
      <div class='nav-wrapper container'> 
      <h4 class='brand-logo center light' >Dashboard.</h4>
      <ul class='right hide-on-med-and-down'>
      </ul>
      </div>
      </nav>
	  </div>
      <div class='section no-pad-bot' id='index-banner'>
      <div class='container'>
      <br><br><br>
      <div class='row center'>
        <h4 class='header col s12 light' style='margin-top:-50px;'>Congratulations !!! </h4> 
        <h5>Your credentials are accepted by the 
        <br><a href='/login'>Click here</a> to login.</h5>
	  </div>";
	}
	else{
			header("location:/system.config");
	   }
	}
});
$Router->post('/changepin',function() use ($Service) { 
	if($Service->Prote()->DBI()->Func()->data()->getpin()==$_POST['opin'])
	{
		if($_POST['npin1']==$_POST['npin2'])
			if($Service->Prote()->DBI()->Func()->data()->change_pin($_POST['npin1']))
				echo "<b>Pin has been changed succesfully</b><br><a href='/settings>Settings</a>'";
		else
		{ 
		 echo "<b>Pins don't match.Try again !!!</b><br>Please  wait...";
		 header("refresh:1;url=/settings");		
		}
	}
	else
	{
		echo "<b>Wrong pin...</b><br>Please  wait...";
		header("refresh:1;url=/settings");
	}

}); 
$Router->get('/changepwd',function() use ($Service) {
		$email=$Service->Prote()->DBI()->Func()->data()->getEmail();
		$pwd="Abhishek123";
		crypt($pwd, $email.'5UCK5'.$pwd); 
		if($id=$this->$Service->Prote()->DBI()->Func()->data()->verify($email,$pwd))
		 echo "Correct";
	echo $Service->Prote()->DBI()->Func()->data()->getpwd();
});

$Router->post('/logout',function() use ($Service) {
	if($Service->Auth()->logout($_POST['auth_token'])){
		    header("location:/login");
		}
		else{
			echo "Could not log out.";
		}
});
//automated-session-logout
$Router->get('/automated-session-logout',function() use ($Service) {
	//This is a bit insecure.So, either eliminate or MODIFY.
	if($Service->Prote()->DBI()->Func()->data()->exitSessions())
	{
		$act="<b>System on automated logout due to inactivity.";
	    $Service->Prote()->DBI()->Func()->activity()->add($act); 	 
		header("location:/login");
	}
	else
	{
	  $act="<b>An automatic session logout failed. Please logout and login to fix.";
	  $Service->Prote()->DBI()->Func()->activity()->add($act); 	
	  header("location:/");
	}

});
$Router->post('/addnote',function() use ($Service) {
	if($Service->Auth()->logged_in()&&$_POST['todo']){
		if($Service->Prote()->DBI()->Func()->notes()->add($_POST['todo']))
		  header("location:/dashboard") ;
		else
			echo "Sorry..Something went wrong.!!";
	}
	      header("location:/dashboard") ;
	
});

$Router->post('/addcom',function() use ($Service) {
	$desc='Comment';
	if($Service->Auth()->logged_in()&&$_POST['comment']){
		if($Service->Prote()->DBI()->Func()->data()->add($desc,$_POST['comment']))
		  header("location:/dashboard") ;
		else
			echo "Sorry..Something went wrong.!!";
	}
	      header("location:/dashboard") ;
	
}); 
/*D1 aree*/
$Router->post('/addcomment',function() use ($Service) {
	if($Service->Auth()->logged_in()&&$_POST['comment']){
		if($Service->Prote()->DBI()->Func()->comment()->add($_POST['title'],$_POST['comment'],$_POST['type']))
		{ 
			echo"
			    <html>  
    			<title>The Story</title>
    			<link rel='stylesheet' href='Static/VA/css/foundation.css' /> 
    			<br>
    			<div class='large-8 medium-8 columns'>
           	    <div class='row'>
                <div class='large-12 columns'>
            	<div class='callout panel' style='box-shadow:2px 4px 3px #eee;'>
            	<h2><b>Post successfull!</b></h2> <br>
                <p><strong>What do you want do now ?</strong>
                <ul>
                <li><b>View all posts</b>
                 <ul>
                  <li><a href='/technical' style='font-weight:600;text-shadow:1px 2px 1px #ddd;'>Technical</a></li>
                  <li><a href='/dev-site' style='font-weight:600;text-shadow:1px 2px 1px #ddd;'>This-site</a></li> 
 				 </ul>
                </li>
				<li><b>Other links:</b><br>
				 <ul>
				 <li><a href='/dashboard' style='font-weight:600;text-shadow:1px 2px 1px #ddd;'>Dashboard</a></li>
				 </ul>
				</li>
                </ul>
                </p>
            	</div>
          		</div>
        		</div> 
      			</div>	 
     			</html>
  			 ";
  			 $act="New post(".$_POST['type'].")<br>@".date('H').date('i');
  			 $Service->Prote()->DBI()->Func()->notification()->add($act);
		}
	 else
			echo "Sorry..Something went wrong.!!";
	}
	else
	{ 
	 echo "<h3>Can't take a test without body.</h3><b>Invalid input!!!<br>Please wait...</b>";
	 header("refresh:2;url=/dashboard"); 
	}
});

$Router->get('/diary',function() use ($Service) {
	if($Service->Auth()->logged_in()){ 
	 //include('Views/VA/diary-auth.html');
	include('Views/VA/diary.php');	
	}
	else
	 header("location:/login"); 
});

$Router->get('/technical',function() use ($Service) {
	 include('Views/VA/technical.php');	 
});
$Router->get('/dev-site',function() use ($Service) {
	 include('Views/VA/thisite.php');	 
}); 
/*Ends*/

$Router->post('/del',function() use ($Service) {
	if($Service->Auth()->logged_in()){
	 if($Service->Prote()->DBI()->Func()->notes()->set_delflag(1,$_POST['id']))   	 
		   if($Service->Prote()->DBI()->Func()->notes()->remove()) 
		  	header("location:/dashboard") ; 
		else
			echo "Sorry..Something went wrong.!!";
	}
});
$Router->post('/delc',function() use ($Service) {
	if($Service->Auth()->logged_in()){
	 if($Service->Prote()->DBI()->Func()->data()->set_delflag(1,$_POST['id']))   	 
		   if($Service->Prote()->DBI()->Func()->data()->remove()) 
		  	header("location:/dashboard") ; 
		else
			echo "Sorry..Something went wrong.!!";
	}
});
$Router->post('/delrem',function() use ($Service) {
	if($Service->Auth()->logged_in()){
	  if($Service->Prote()->DBI()->Func()->reminder()->delete($_POST['reminderid'])) 
		  	header("location:/dashboard") ; 
		else
			echo "Sorry..Something went wrong.!!";
	}
});
$Router->post('/delalarm',function() use ($Service) {
	if($Service->Auth()->logged_in()){
	  if($Service->Prote()->DBI()->Func()->alarm()->removeAllAlarms()) 
		  	header("location:/dashboard") ; 
		else
			echo "Sorry..Something went wrong.!!";
	}
});
$Router->post('/delnoti',function() use ($Service) {
	if($Service->Auth()->logged_in()){
	  if($Service->Prote()->DBI()->Func()->notification()->remove($_POST['notid'])) 
		  	header("location:/dashboard") ; 
		else
			echo "Sorry..Something went wrong.!!";
	}
});
$Router->post('/delAllnoti',function() use ($Service) {
	if($Service->Auth()->logged_in()){
	  if($Service->Prote()->DBI()->Func()->notification()->removeAll()) 
		  	header("location:/dashboard") ; 
		else
			echo "Sorry..Something went wrong.!!";
	}
});
$Router->post('/delfile',function() use ($Service) {
	if($Service->Auth()->logged_in()){ 
		if(file_exists($_POST['path'].$_POST['name']))
		{ 
			//Create a temp
			if(!is_dir("Views/VA/temp"))
			{
              if(mkdir("Views/VA/temp"))
              {
                $file=fopen($_POST['path'].$_POST['name'], "r") or die("Unable to locate the file."); 
                fclose($file);
                $data=file_get_contents($_POST['path'].$_POST['name']); 
                $temp=fopen("Views/VA/temp/temp".date("d").date('s'), "w") or die("Unable to locate the file.");  
                fwrite($temp,"/*Data saved from the file".$_POST['name']."\nParent file: ".$_POST['name']."\nDeleted on : ".date('D')."/".date('m')." ".date('H').":".date('i')."*/\n------------------------".$data);
                fclose($temp);
              }
              else
              { 
              	echo "Unable to create directory.";
              }
			}
			else
			{
				$file=fopen($_POST['path'].$_POST['name'], "r") or die("Unable to locate the file."); 
                fclose($file);
                $data=file_get_contents($_POST['path'].$_POST['name']);
                $temp=fopen("Views/VA/temp/temp".date("d").date('s'), "w") or die("Unable to locate the file.");
                fwrite($temp,"/*Data saved from the file".$_POST['name']."\nParent file: ".$_POST['name']."\nDeleted on : ".date('D')."/".date('m')." ".date('H').":".date('i')."*/\n------------------------".$data);
                fclose($temp);
			}  
			$file=$_POST['path'].$_POST['name'];
			unlink($file); 
			$file=$_POST['name'];//Only name..no path
			$act=$file." deleted.<br>@".date('H').":".date('i');
			$Service->Prote()->DBI()->Func()->notification()->add($act);
			echo "<html>  
    			<title>File exists</title>
    			<link rel='stylesheet' href='Static/VA/css/foundation.css' /> 
    			<br>
    			<div class='large-8 medium-8 columns'>
           	    <div class='row'>
                <div class='large-12 columns'>
            	<div class='callout panel' style='box-shadow:2px 4px 3px #eee;'>
            	<h2><b>File removed!!!</b></h2> <br>
                <p><strong>Notification of this deletion is added.</strong></p>
                <ul>
                <b><u>Details:</u></b>
                <li><b>Filename: ".$_POST['name']."</b></li>
                <li><b>Deletion time: ".date('H').":".date('i')."</b></li>
                </ul>
                <a href='/dashboard'><b>Dashboard</b></a>
            	</div>
          		</div>
        		</div> 
      			</div>	 
     			</html> 
			";
			header("refresh:30;url=/dashboard");
		}
		else
		{
			echo "<b>File (<i>".$_POST['name']."</i>) doesn't exist.</b><br><a href='/dashboard'>Dashboard</a>"; 
			header("refresh:30;url=/dashboard");
		}
	} 
		else
			echo "Error 404,file not found.";; 
});

$Router->post('/disdel',function() use ($Service) {
	if($Service->Auth()->logged_in()&&$_POST['val']==$Service->Prote()->DBI()->Func()->data()->getpin()){
	 if($Service->Prote()->DBI()->Func()->notes()->set_Handle()) 
	 {    
	   header("location:/dashboard") ; 
	 }
		else
			echo "Sorry..Something went wrong.!!";
	}
	else
	{

		echo "<h2>Wrong password !!!</h2>Please wait..";
		$act="Wrong pin.<br>@".date('H').":".date('i');
		$Service->Prote()->DBI()->Func()->notification()->add($act);
		header("refresh:1;url=/dashboard");
	}
});
$Router->get('/lock-the-system-for-reed-access-only',function() use ($Service) {
	if($Service->Auth()->logged_in()){
	 if($Service->Prote()->DBI()->Func()->notes()->set_Handle()) 
	 {    
	   header("location:/dashboard") ; 
	 }
		else
			echo "Sorry..Something went wrong.!!";
	}
});

$Router->post('/store',function() use ($Service) {
	if($Service->Auth()->logged_in()){ 
		if($_POST['email']&&$_POST['pwd']&&$_POST['website'])
        {
        	if($Service->Prote()->DBI()->Func()->acct()->add($_POST['website'],$_POST['email'],$_POST['pwd']))
		  	 header("location:/dashboard") ; 
		}
		else
			echo "Sorry..Something went wrong.!!";
	}
});
$Router->post('/addsite',function() use ($Service) {
	if($Service->Auth()->logged_in()){ 
		if($_POST['web']&&$_POST['weburl'])
        {
        	if($Service->Prote()->DBI()->Func()->web()->add($_POST['web'],$_POST['weburl']))
		  	 header("location:/dashboard") ; 
		}
		else
			echo "Sorry..Something went wrong.!!";
	}
});

$Router->post('/remind',function() use ($Service) {
	if($Service->Auth()->logged_in()){   
		if(!is_numeric($_POST['hour'])||!is_numeric($_POST['min']))
		{
			echo "<h3>Invalid input obtained.</h3>Exiting...<br>
			<a href='/dashboard'>Dashboard.</a>";
			exit(); 
		}
		if($_POST['rtext']&&$_POST['hour']&&$_POST['min'])
        {
        	if(($_POST['hour']<0||$_POST['hour']>23)||($_POST['min']<0||$_POST['min']>59))
        	{
              echo "Invalid values";
              exit();
        	}  
        	else if($_POST['hour']==date('H')&&$_POST['min']<=date('i')) 
        	{ 	
           	 header("location:/dashboard") ; 
        	}
            else if($Service->Prote()->DBI()->Func()->reminder()->add($_POST['rtext'],$_POST['hour'],$_POST['min'],$_POST['linkname']))
           	 header("location:/dashboard") ;    
		}
		else
			echo "Sorry..Something went wrong.!!";
	}
});
$Router->post('/addalarm',function() use ($Service) {
	if($Service->Auth()->logged_in()){  
		if(!is_numeric($_POST['ah'])||!is_numeric($_POST['am']))
		{
			echo "<h3>Invalid input obtained.</h3>Exiting...<br>
			<a href='/dashboard'>Dashboard.</a>";
			exit();
		}
		if( $_POST['ah']&&$_POST['am'])
        {
        	if(($_POST['ah']<0||$_POST['ah']>23)||($_POST['am']<0||$_POST['am']>59))
        	{
              echo "<h3>Invalid values</h3>";
              exit();
        	}  
        	else if($_POST['ah']==date('H')&&$_POST['am']<=date('i')) 
        	{ 	
           	 header("location:/dashboard") ; 
        	}
            else if($Service->Prote()->DBI()->Func()->alarm()->add($_POST['ah'],$_POST['am']))
		  	 header("location:/dashboard") ; 
		}
		else
			echo "Sorry..Something went wrong.!!";
	}
}); 
$Router->post('/alarm',function() use ($Service) {
	if($Service->Auth()->logged_in()){  
		if(!is_numeric($_POST['offset']))
		{
			echo "<h3>Invalid input obtained.</h3>Exiting...<br>
			<a href='/dashboard'>Dashboard.</a>";
			exit();
		}
		if( $_POST['offset'])
        { 
        	if($_POST['drop']=='h')
        	{
        		if($_POST['offset']>0&&$_POST['offset']<23)
        		{
        		 $res=date('H')+$_POST['offset'];
        		 if($res>23)
        		 {
        		 	$res=$res-24;
        		 }
        		 if($Service->Prote()->DBI()->Func()->alarm()->add($res,date('i')))
        		 	 header("location:/dashboard") ;
        		}
        		else
        		{
        			echo "Invalid data values....<br>Please wait..";
        			header("refresh:3;url=/");
        		}
        	}
        	else if($_POST['drop']=='m')
        	{
        		if($_POST['offset']>0)
        		{
        		 $minutes=date('i')+$_POST['offset']; 
        		 if($minutes==60)
        		 { 
        		 	$hours=date("H");
        		 	$minutes=0;
        		 }
        		 else if($minutes>60)
        		 { 
        		 	$temp=$minutes;
        		 	$minutes=($minutes%60);//e.g 121 minutes will be made as (121%60)=1
        		 	$hours=date("H")+($temp/60);
        		 } 	
        		 else 
        		   $hours=date('H'); 
        		 if($Service->Prote()->DBI()->Func()->alarm()->add($hours,$minutes))
        		   header("location:/dashboard") ; 
        		}
        	}
		}
		else
			echo "Sorry..Something went wrong.!!";
	}
}); 
$Router->post('/addthought',function() use ($Service) {
	if($Service->Auth()->logged_in()){   
		if($_POST['thought']&&$_POST['author'])
		{
		  if($Service->Prote()->DBI()->Func()->thought()->add($_POST['thought'],$_POST['author']))
            header("location:/dashboard") ; 	
		}
		}
		else
			echo "Sorry..Something went wrong.!!";  
});  
$Router->post('/autotimemod',function() use ($Service) {
	if($Service->Auth()->logged_in()){   
		if($_POST['timecount']>0&&$_POST['timecount']<=5)
		{
		  if($Service->Prote()->DBI()->Func()->data()->update($_POST['timecount']))
            header("location:/settings") ; 	
		}
		else
			echo "Not your cup of tea. :)";
		}
		else
			echo "Sorry..Something went wrong.!!";  
}); 
$Router->post('/event',function() use ($Service) {
	if($Service->Auth()->logged_in()){   
		if($_POST['emonth']<=12&&$_POST['emonth']>=1&&$_POST['etext'])
		{ 
			if($_POST['eday']>0&&$_POST['eday']<=31)
			{  	 
			 if($_POST['etype']=="one_time" || $_POST['etype']=="all_time")
			 { 
			  if($Service->Prote()->DBI()->Func()->event()->add($_POST['etext'],$_POST['eday'],$_POST['emonth'],$_POST['etype']))
			  	echo "Event successfully added.<br>Please wait...";
			   header("refresh:1;url=/dashboard") ; 
		     }
		     else echo "Try something better !!!";
			}
			else
			 header("location:/dashboard") ; 		
		}
		else 
		   header("location:/dashboard") ; 	
		}
		else
			echo "Sorry..Something went wrong.!!";  
});  

$Router->post('/html',function() use ($Service) {
	if($Service->Auth()->logged_in()){ 
		if($_POST['filename']==""&&$_POST['code']=="")
		{
		  header("location:/testDir");	
		} 
		else if($_POST['code']=='Remove'||$_POST['code']=='remove')
		{ 
	      if(file_exists("Views/VA/custom/".$_POST['filename']))
	      { 
           if(unlink("Views/VA/custom/".$_POST['filename']))
           { 
            echo "<h3>File '".$_POST['filename']."' removed successfully.<br><br>
                  <a href='/dashboard' style='padding:7px;background:#ee6e73;text-decoration:none;color:#fff;font-size:19px;'>
	               Dashboard</a></h3>
            ";
            $act=$_POST['filename']." removed";
            $notification=$_POST['filename']." removed<br>@".date('H').":".date('i');
            $Service->Prote()->DBI()->Func()->activity()->add($act);
            $Service->Prote()->DBI()->Func()->notification()->add($notification); 
           }
          }
          else
          	echo "No such file exists.";
		}
	   	else if($_POST['filename']&&$_POST['code'])
		{ 

		  $name=$_POST['filename'];
		  $name=preg_replace ("/ +/", " ", $name);
		  //Prevent injections.Save the planet.
		  $invalid_sym=array("<",">","?");
		  foreach ($invalid_sym as $sym) {
		  	if(strpos($name, $sym)!==false)
		  	{
		      echo "<h3>Invalid filename.</h3><h5>Terminating...</h5><a href='/dashboard'>Dashboard.</a>";
		  	  exit();		
		  	}
		  }
		  if($name==" ")
		  {
		  	echo "<h3>Invalid filename.</h3><h5>Terminating...</h5><a href='/dashboard'>Dashboard.</a>";
		  	exit();
		  }
		  if(file_exists("Views/VA/custom/".$_POST['filename']))
		  { 
		  	echo " <html>  
    			<title>File exists</title>
    			<link rel='stylesheet' href='Static/VA/css/foundation.css' /> 
    			<br>
    			<div class='large-8 medium-8 columns'>
           	    <div class='row'>
                <div class='large-12 columns'>
            	<div class='callout panel' style='box-shadow:2px 4px 3px #eee;'>
            	<h2><b>File already exists!!!</b></h2> <br>
                <p><strong>What do you want do now ?</strong>
                <ul>
                <li><b><a href='".$_POST['filename']."' target='_new'>Open</a> the file.</b></li>
                <li><form action='/delfile' method='post'>
                     <input type='hidden' name='name' value='".$_POST['filename']."'>
                     <input type='hidden' name='path' value='Views/VA/custom/'>
                    <button type='submit' href='Views/VA/custom/".$_POST['filename']."' style='color:#f00;background:none;padding:0;margin:0;'><b>Delete</b></button> the file.
                    </form>
                </li>
				<li><b>Other links:</b><br>
				 <ul>
				 <li><a href='/dashboard' style='font-weight:600;text-shadow:1px 2px 1px #ddd;'>Dashboard</a></li>
				 </ul>
				</li>
                </ul>
                </p>
            	</div>
          		</div>
        		</div> 
      			</div>	 
     			</html>
  			 "; 
		  }
		  else
		  { 	
		   $myfile = fopen("Views/VA/custom/".$name, "w") or die("Unable to locate the file."); 
 		   $txt = "<html>".$_POST['code']."</html>";  
		   fwrite($myfile, $txt);
		   fclose($myfile);  
		   header("location:Views/VA/custom/$name");
		  }
		}
		else
		{ 
			echo "<h3>Invalid Value input.</h3>Terminating...<br><a href='/dashboard'>Dashboard.</a>";
		  	exit();
		}
	}
	else
     echo "Sorry..Something went wrong.!!";  
});
$Router->get('/testDir',function() use ($Service) {
	if($Service->Auth()->logged_in()){  
	  if ($handle = opendir('Views/VA/custom')) { 
	   echo "<h2>List of test files.</h2><a href='/dashboard' style='padding:7px;background:#ee6e73;text-decoration:none;color:#fff;font-size:19px;'>
	  	Dashboard</a><br><br>";
       while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != "..") { 
            echo "<img src='Static/VA/images/chrome.jpg' width='17'>
                  <a href='Views/VA/custom/$file' style='text-decoration:none;font-size:20px;padding:7px;'>$file</a><br>";
        }
    }
    echo "";
    closedir($handle);
}	
	}
	else
     header("location:/");  
});
//for the posts
$Router->get('/technical/([A-Za-z0-9]+.+)',function() use ($Service) {
	    $header=$_SERVER['REQUEST_URI'];
	    $header=preg_replace ("/%20/"," ",$header);
	    $c=$Service->Database()->find_many("SELECT * from diary where header like '".substr($header,11)."';"); 
	    echo " 
	    <meta name='viewport' content='width=device-width, initial-scale=1.0' />
	    <title>Tehnical stuff</title>
	    <link rel='stylesheet' href='/Static/VA/css/foundation.css' /> 
	    <link rel='shortcut icon' href='/Static/VA/images/diary.png' type='image/x-icon'>
	    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	    </head>
		<body>
	    <div class='row'>
	    <div class='large-12 columns'>
	    <h1 align='left'>".substr($header,11)."</h1><br>
	    </div>
	    </div>
	    <style>
	    .panel{
	      font-family: 'Open Sans', sans-serif;
	    }
	    </style> 
		<div class='row'>
	      <div class='large-8 medium-8 columns'>
	         <div class='row'>";
	         foreach ($c as $data)
	         echo " <div class='large-12 columns' style='background:#ECFAFF;padding-top:1rem;'>".$data->text."</div>";
	        echo "</div>
	       </div>
	     </div>   
	      ";  
	    $c=$Service->Database()->find_many("SELECT * from diary where header like '".$header."' ");  
             if($c)
             {  
              foreach ($c as $data)
               {
                   echo "<section id='".$data->header."'></section>";
                   echo "<div class='callout panel'><h5>".$data->header."</h5>".$data->text."<strong>";  ;
                 echo  substr($data->time,10,6)." hrs, ". $Service->Prote()->DBI()->Func()->comment()->get_access_date_month($data->cid)." ".$Service->Prote()->DBI()->Func()->comment()->get_access_date_day($data->cid).", ".$Service->Prote()->DBI()->Func()->comment()->get_access_year($data->cid)."</strong> </div>";
               }
             } 
             /*else
              header("location:/unknown");*/
});

//The activity log.
$Router->get('/activity.log',function() use ($Service) {
	if($Service->Auth()->logged_in()){ 
      if($Service->Prote()->DBI()->Func()->activity()->countActivity()==0)
      {
      	echo "<title>Activity Log.</title><h3><u>Activity log :  </u></h3>No activity to display.<br><a href='/dashboard' style='text-decoration:none;font-weight:600;'>&laquo; Dashboard</a>";
      	exit();  
      }
	  $c=$Service->Database()->find_many("SELECT * from activity order by time"); 
	  echo "<title>Activity Log.</title><h3><u>Activity log :  </u></h3><ul>"; 
          foreach ($c as $data)
          { 
          	echo "<li style='font-size:19px;padding:6px;line-height:0.6;'>".$data->act_des." @".$data->time."</li>";
          }
     echo "</ul>"; 
     echo "<form action='/activity.log/clear_activity_log' method='post'><a href='/dashboard' style='text-decoration:none;font-weight:600;'>&laquo; Dashboard</a><button type='submit'>Clear</button></form>";    
	}
	else
     header("location:/");  
});
$Router->post('/activity.log/clear_activity_log',function() use ($Service) {
	if($Service->Auth()->logged_in()){   
		if($Service->Prote()->DBI()->Func()->activity()->remove())
         header("location:/activity.log"); 
      }
	else
     header("location:/");  
});
//The initial customization 
$Router->get('/system.config',function() use ($Service) { 
	$q=$Service->Database()->find_one("Select count(*) as user from admin;");
        //echo $q->user; 
        $usercount=$q->user;
        if($usercount==0)
	     include("Views/VA/config.html");
	    else
	     header("location:/login");	
});
$Router->get('/system.config-initial',function() use ($Service) { 
	$q=$Service->Database()->find_one("Select count(*) as user from admin;"); 
        $usercount=$q->user;
        if($usercount>0)
         header("location:/login");
        else  
         header("location:/system.config");
});

/*
$Router->post('/restore',function() use ($Service) {
	if($Service->Auth()->logged_in()){   	 
		   if($Service->Prote()->DBI()->Func()->notes()->restore()) 
		  	header("location:/dashboard") ; 
		else
			echo "Sorry..Something went wrong.!!";
	}
});*/
